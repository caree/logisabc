create table T_ACTION_INFO
(
    action_code varchar(32) primary key
    ,action_name varchar(128)
    ,action_note varchar(1024)
    , unit varchar(32)
    ,system_id varchar(32)
);
create table T_ACTION_INFO2 as select action_code, action_name, action_note, system_id from  T_ACTION_INFO
drop table if exists T_ACTION_INFO
alter table T_ACTION_INFO2 rename to T_ACTION_INFO
insert into T_ACTION_INFO2(action_code, action_name, action_note, system_id, unit) select action_code, action_name, action_note, system_id, '批次' from  T_ACTION_INFO
alter table T_ACTION_INFO2 rename to T_ACTION_INFO
select action_code, action_name, action_note, action_work_capability, system_id  
    from  T_ACTION_INFO where action_code = '';
update T_ACTION_INFO set
		action_name = '', action_note = '',
		action_work_capability = '',
        system_id = '' 
	 where action_code = '';
insert into T_ACTION_INFO
	(action_code, action_name, action_note, action_work_capability, system_id)
	values
	('1000', '进货', '', 0, 'inventory');


--用户的作业处理量
create table T_ACTION_LINK_WORK_CAPABILITY(
    action_code varchar(32)
    , user_id varchar(32)
    , action_work_capability int
    , primary key(action_code, user_id)
)
insert into T_ACTION_LINK_WORK_CAPABILITY(action_code, user_id, action_work_capability, unit) select action_code, user_id, action_work_capability, '批次' from T_ACTION_LINK_WORK_CAPABILITY
alter table T_ACTION_LINK_WORK_CAPABILITY2 rename to T_ACTION_LINK_WORK_CAPABILITY
replace into T_ACTION_LINK_WORK_CAPABILITY(action_code, user_id, action_work_capability) values('1000', 'user1', 1);
replace into T_ACTION_LINK_WORK_CAPABILITY(action_code, user_id, action_work_capability) values('1000', 'user2', 2);
--投入要素
create table T_INVENTORY_RESOURCE
(
    resource_id varchar(32) primary key
    ,resource_name varchar(128)
    ,resource_note varchar(1024)
);
create table T_INVENTORY_RESOURCE2 as select resource_id, resource_name, resource_note from T_INVENTORY_RESOURCE
drop table if exists T_INVENTORY_RESOURCE
alter table T_INVENTORY_RESOURCE2 rename to T_INVENTORY_RESOURCE
alter table T_INVENTORY_RESOURCE add col primary key (resource_id)

insert into T_INVENTORY_RESOURCE2 select resource_id, resource_name, resource_note from T_INVENTORY_RESOURCE 

select resource_id, resource_name, resource_note, resource_selected, resource_fee 
	from T_INVENTORY_RESOURCE where resource_id like '1%';
update T_INVENTORY_RESOURCE set resource_fee =  where resource_id = '';
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_fee)
        values('10001','正规职员',
         ' 贵公司的正式员工。正式员工，稍微从事物流业务的话，作为投入要素进行选择。', 0);

create table T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE(
    resource_id varchar(32)
    , user_id varchar(32)
    , resource_fee int
    , primary key(resource_id, user_id)
)
select T1.resource_id, T1.resource_name, T1.resource_note, ifnull(T3.resource_fee, 0) resource_fee
    from T_INVENTORY_RESOURCE T1,T_INVENTORY_LINK_ITEM T2 
    left join T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE T3 on T1.resource_id = T3.resource_id and T3.user_id = ''
     where T1.resource_id = T2.resource_id and T2.resource_group = '20000'

select T1.resource_id, T2.resource_group, ifnull(T3.resource_fee, 0) resource_fee
    from T_INVENTORY_RESOURCE T1,T_INVENTORY_LINK_ITEM T2, T_INVENTORY_RESOURCE_GROUP T3
    left join T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE T3 on T1.resource_id = T3.resource_id and T3.user_id = ''
     where T1.resource_id = T2.resource_id and T2.resource_group = T3.resource_group_id and T3.system_id = ''



--各投入要素使用量
create table T_INVENTORY_RESOURCE_ACTION_USAGE
(
    resource_id varchar(32) 
    ,action_code varchar(32)
    , user_id varchar(32)
    ,usage integer 
    ,primary key(resource_id, action_code, user_id)
)
select * from T_INVENTORY_RESOURCE_ACTION_USAGE where resource_id = '' and action_code = ''
insert into T_INVENTORY_RESOURCE_ACTION_USAGE(resource_id, action_code, usage)
	 values('', '', );
update T_INVENTORY_RESOURCE_ACTION_USAGE set usage = 
	where resource_id = '' and action_code = '';

select T1.action_code, T1.action_name, T1.action_note, ifnull(T2.usage, 0) usage 
                    from  T_ACTION_INFO T1 left join T_INVENTORY_RESOURCE_ACTION_USAGE T2 on T1.action_code = T2.action_code 
                    where T1.system_id = '' and T2.user_id = ''
-- 建立投入要素类型与类型的具体项目之间的管理
-- 目前要素类型定义为 人工(10000)、空间(20000)、机器设备(30000)、原材料（40000）四种
create table T_INVENTORY_LINK_ITEM
    (
        resource_group varchar(32)
        ,resource_id varchar(32)
        ,primary key(resource_id, resource_group)
    )
select resource_id from T_INVENTORY_LINK_ITEM where resource_group in();
-- 投入要素表
-- 要素要有所处系统的区别，比如仓储部分的投入要素和运输部分的投入要素
create table T_INVENTORY_RESOURCE_GROUP(
    resource_group_id varchar(32) primary key
    ,resource_group_name varchar(32)
    ,note varchar(1024)
    ,system_id varchar(32)
)
select resource_group_id, resource_group_name, system_id from T_INVENTORY_RESOURCE_GROUP where system_id = 'inventory';
insert into T_INVENTORY_RESOURCE_GROUP(resource_group_id, resource_group_name, system_id, note)
    values('10000', '人工要素', 'inventory', '人工要素');





create table T_ACTION_COST_HISTORY(
    history_id varchar(32)
    , user_id varchar(32)
    , time_stamp varchar(32)
    , primary key(history_id, user_id)
)
create table T_ACTION_COST_HISTORY2 select history_id, user_id, time_stamp from T_ACTION_COST_HISTORY
insert into T_ACTION_COST_HISTORY select history_id, 'user1', time_stamp from T_ACTION_COST_HISTORY2  
delete from T_ACTION_COST_HISTORY where history_id = '';
replace into T_ACTION_COST_HISTORY(history_id, time_stamp) values('', '');
select history_id, time_stamp from T_ACTION_COST_HISTORY  where history_id = '' 
-- 保存历史数据中作业的数据
create table T_ACTION_LINK_HISTORY(
    history_id varchar(32),
    action_code varchar(32),
    user_id varchar(32),
    action_fee int,
    primary key(history_id, action_code, user_id)
)
create table T_ACTION_LINK_HISTORY2 as select history_id, action_code, action_fee from T_ACTION_LINK_HISTORY
insert into T_ACTION_LINK_HISTORY select history_id, action_code, 'user1', action_fee from T_ACTION_LINK_HISTORY2
replace into T_ACTION_LINK_HISTORY(history_id, action_code, action_fee) values('', '', '');
select T1.history_id, T1.action_code, T1.action_fee, T2.action_name  from T_ACTION_LINK_HISTORY T1, T_ACTION_INFO T2 where T1.history_id = '' and T1.action_code = T2.action_code   
select count(action_code) count from T_ACTION_LINK_HISTORY where history_id = ''
delete from T_ACTION_LINK_HISTORY where history_id = ''


create table T_PRODUCT(
    product_id varchar(32) primary key
    , product_name varchar(128)
    , note varchar(1024)
    , quantity int default 1
)
replace into T_PRODUCT(product_id, product_name, note) values('', '', '');
delete from T_PRODUCT where product_id = '';
select product_id, product_name, note from T_PRODUCT where product_id = '';


create table T_PRODUCT_LINK_INFO(
    product_id varchar(32) 
    , action_code varchar(32)
    , user_id varchar(32)
    , action_count int default 0
    , primary key(product_id, action_code, user_id)
)


create table T_PRODUCT_LINK_INFO2 as select product_id, action_code, action_count from T_PRODUCT_LINK_INFO
insert into T_PRODUCT_LINK_INFO select product_id, action_code, 'user1', action_count from T_PRODUCT_LINK_INFO2
replace into T_PRODUCT_LINK_INFO(product_id, action_code, action_count) values('', '', );
delete from T_ACTION_INFO where product_id = '';

select T1.product_id, T1.product_name, T1.note, ifnull(T2.action_count, 0) action_count 
    from T_PRODUCT T1, T_PRODUCT_LINK_SYSTEM T3 
     left join T_PRODUCT_LINK_INFO T2  on T1.product_id = T2.product_id 
      where T1.product_id = T3.product_id and T3.system_id = 'inventory'


create table T_PRODUCT_LINK_SYSTEM(
    product_id varchar(32) primary key,
    system_id varchar(32)
)
insert into T_PRODUCT_LINK_SYSTEM(product_id, system_id) values('', '');
delete from T_PRODUCT_LINK_SYSTEM where product_id = '';

select from T1.product_id, T1.product_name T_PRODUCT T1, T_PRODUCT_LINK_SYSTEM T2 where T1.product_id == T2.product_id and T2.system_id = '';

--系统通用设置项的存储
create table T_GENERAL_SETTINGS(
    setting_id varchar(32) primary key,
    setting_value varchar(32)
)
--
select setting_id, setting_value from T_GENERAL_SETTINGS
replace into T_GENERAL_SETTINGS(setting_id, setting_value) values('user_set', '_default');
replace into T_GENERAL_SETTINGS(setting_id, setting_value) values('user_data_set', '_default');
replace into T_GENERAL_SETTINGS(setting_id, setting_value) values('system_set', '_default');

-- 用户列表
CREATE TABLE THINK_USER (
          ACCOUNT varchar(100) NOT NULL,
          PASSWORD varchar(32) NOT NULL,
          REMARK varchar(255)  NULL,
          status varchar(32) default 'no',
          PRIMARY KEY  (ACCOUNT)
);
insert into THINK_USER(ACCOUNT, PASSWORD, REMARK) values('admin', '202cb962ac59075b964b07152d234b70', '管理员');
insert into THINK_USER(ACCOUNT, PASSWORD, REMARK) select ACCOUNT, PASSWORD, REMARK from THINK_USER2

--班级管理
create table T_CLASSES(
    class_id varchar(32) primary key,
    class_name varchar(32),
    create_time varchar(32),
    note varchar(1024)
)
select class_id, class_name, create_time, note from T_CLASSES order by create_time desc;
replace into T_CLASSES(class_id, class_name, create_time, note) values('1','2','3','4');

create table T_CLASS_LINK_USER(
    class_id varchar(32),
    user_id varchar(32),
    primary key(class_id, user_id)
)
replace into T_CLASS_LINK_USER(class_id, user_id) values('','');
-------------------------------------------------------------------------------

SELECT  T1.action_code, T1.resource_id, T1.usage FROM T_INVENTORY_RESOURCE_ACTION_USAGE T1 , T_ACTION_INFO T2 
where T1.action_code = T2.action_code and T2.system_id = 'inventory'

select T1.resource_id, T1.resource_fee from T_INVENTORY_RESOURCE T1 ,T_INVENTORY_LINK_ITEM T2, T_INVENTORY_RESOURCE_GROUP T3
where T1.resource_id = T2.resource_id and T2.resource_group = T3.resource_group_id and T3.system_id = 'inventory'
-------------------------------------------------------------------------------
insert into T_INVENTORY_RESOURCE_GROUP(resource_group_id, resource_group_name, system_id, note)
    values('20000', '空间要素', 'inventory', '空间要素');
insert into T_INVENTORY_RESOURCE_GROUP(resource_group_id, resource_group_name, system_id, note)
    values('30000', '机器设备', 'inventory', '机器设备');
insert into T_INVENTORY_RESOURCE_GROUP(resource_group_id, resource_group_name, system_id, note)
    values('40000', '原材料', 'inventory', '原材料');
-------------------------------------------------------------------------------
insert into T_ACTION_INFO
    (action_code, action_name, action_note, action_work_capability, system_id)
    values
    ('2000', '保管品的整理', '', 0, 'inventory');
insert into T_ACTION_INFO
    (action_code, action_name, action_note, action_work_capability, system_id)
    values
    ('3000', '出货', '', 0, 'inventory');
insert into T_ACTION_INFO
    (action_code, action_name, action_note, action_work_capability, system_id)
    values
    ('4000', '流通加工', '', 0, 'inventory');
insert into T_ACTION_INFO
    (action_code, action_name, action_note, action_work_capability, system_id)
    values
    ('5000', '退货', '', 0, 'inventory');
insert into T_ACTION_INFO
    (action_code, action_name, action_note, action_work_capability, system_id)
    values
    ('6000', '信息处理', '', 0, 'inventory');
insert into T_ACTION_INFO
    (action_code, action_name, action_note, action_work_capability, system_id)
    values
    ('7000', '管理业务其他', '', 0, 'inventory');
----------------------------------------------------------------------------------------------
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('10001','10000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('10002','10000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('10003','10000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('10004','10000');

insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('20001','20000');

insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30001','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30002','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30003','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30004','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30005','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30006','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30007','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30008','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30009','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30010','30000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('30011','30000');

insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40001','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40002','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40003','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40004','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40005','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40006','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40007','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40008','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40009','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40010','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40011','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40012','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40014','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40015','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40016','40000');
insert into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('40013','40000');
----------------------------------------------------------------------------------------------
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('10002','打工', 
            ' 贵公司雇佣的钟点工和打工者，虽然工资和经费等有差距，但无所谓。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('10003','外部委托（管理员）', 
            ' 首先雇佣外部委托，该设施中作为管理者被派遣的人。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('10004','外部委托（工作者）',
         ' 首先雇佣外部委托，该设施中作为工作者被派遣的人。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('20001',' 空间（土地、建筑）',
         ' 物流业上使用的（土地）（建筑物）','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30001','叉车',
         ' 每个托盘进行车辆装卸，清除上山吊椅等用手装卸托盘的设备','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30002','货架（搁板）',
         ' 架子和颜料等用于保管的长期设置的设备','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30003','运输机',
         ' 将箱子等放在上面移动的设备','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30004','扫描机',
         ' 扫描条形码和检验产品时使用的便携式设备。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30005','数据分类系统',
         '为选择表、引进情报系统、显示颜料和手推车中选择品种、数量等。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30006','纸箱组合机',
         ' 用瓦楞纸组装箱子的设备','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30007','捆包机器',
         '包装用的设备','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30008','检点物品机器',
         '导入验货用的设备','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30009','电话传真',
         ' 为了物理业务的电话、传真。传达发货指示的情况下，作为投入要素选择。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30010','计算机',
         '物流业务电脑。使用制作的选择表、接货书等情况下，作为投入要素选择。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('30011','打印机复印机',
         '物流业务的打印机·复印机。使用选择表和接货书等印字的情况下，作为投入要素选择。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40001','纸张（分类表）',
         '选择表输出的纸。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40002','纸张（交货发票）',
         '交货发票用纸','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40003','纸箱',
         ' 交货用纸，包括卷纸等。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40004','缓冲材料',
         ' 为防止发货时，商品受损，使用坐席、纸、空气管等。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40005','封口材料',
         ' 发货时利用集装箱和纸箱包封（pp带、订书器和胶带等）','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40006','标价签',
         '贴在商品上为了供货使用的标价。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40007','标签',
         ' 价签以外的，商品上贴的为供货时用的标签。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40008','贴标价签的机器',
         '为商品标价签而使用的工具','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40009','贴标签的机器',
         ' 为贴价签而使用的工具','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40010','包装纸',
         ' 流通加工利用的材料，指特定顾客专用的包装纸','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40011','货签',
         '  记录为了供货首先申报的住所、商品名、数量等。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40012','在设施内用于搬运的集装箱',
         ' 区内装卸和用于保管集装箱，折叠集装箱在内。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40013','出货用的集装箱',
         ' 用于出货的集装箱，折叠集装箱在内。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40014','平板',
         ' 区内装卸和保管、用于发货时使用的调色板。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40015','推车',
         ' 载着商品、集装箱、调色盘移动的手装卸的机器。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('40016','周转车',
         ' 为载商品和集装箱移动的手装卸用的设备。','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('50001', '积合航班（特别·一般）',
         ' ','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('50002', '快递',
         ' ','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('50003', '摩托车运输快递',
         ' ','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('50004', '包租卡车',
         ' ','1', 0);
insert into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note, resource_selected, resource_fee)
        values('50005', '自家用汽车',
         ' ','1', 0);
       