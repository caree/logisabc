<?php
require_once('tools.php');


class InventoryGroupAction extends Action
{
    public function inventory_group_for_user_resource_fee_set_index()
    {
        $this->display();
    }
    // 作业设定
    public function user_inventory_group_for_resource_percent_set_index()
    {
        $this->display();
    }
    public function inventory_group_index()
    {
        $this->display();
    }
    public function inventory_group_list()
    {
        $system_id = Tools::get_system_id();
        $sql_select = 
            "select resource_group_id, resource_group_name,note, system_id 
             from T_INVENTORY_RESOURCE_GROUP where system_id = '$system_id';";
        $select_result = Tools::get_query_result($sql_select);
        echo json_encode($select_result);return;
    }
    public function inventory_group_list_user()
    {
        $system_id = Tools::get_system_id();
        $sql_select = "select resource_group_id, resource_group_name,note, system_id from T_INVENTORY_RESOURCE_GROUP 
                        where system_id = '$system_id';";
        $select_result = Tools::get_query_result($sql_select);
        echo json_encode($select_result);return;
    }
    public function remove_group()
    {
        $json_str = Tools::request("data");
        $flag = true;
        $codes = "''";
        if(Tools::json_is_array($json_str)){
            $json = json_decode($json_str,true);

            $sql_delete_items = "";
            for($i = 0; $i < count($json); $i++)
            {
                $action = $json[$i];
                $resource_group_id = $action['resource_group_id'];
                $codes .= ",'".$resource_group_id."'";
            }
        }
        else{
                $action = json_decode($json_str,true);
                $resource_group_id = $action['resource_group_id'];
                $codes .= ",'".$resource_group_id."'";
        }

        // echo $sql_delete; return;
        $sql_select = "select resource_id from T_INVENTORY_LINK_ITEM where resource_group in($codes);";
        $list = Tools::get_query_result($sql_select);
        foreach ($list as $key => $row) {
            $resource_id_array[] = $row['resource_id'];
        }
        // $resource_id_array = array_map(function($row){
        //     return $row['resource_id'];
        // }, $list);

        $sql_delete = "delete from T_INVENTORY_RESOURCE_GROUP
                    where resource_group_id in($codes);";
        $InventoryManageAction = A('InventoryManage');
        $sql_delete .= $InventoryManageAction->get_sql_remove_inventory_item_data($resource_id_array);
        $flag = Tools::trans_sql($sql_delete);

        echo $flag ? 'ok':'failed';
    }
    function check_group_id_exists($group_json, $system_id)
    {
        $codes = "''";

        if(Tools::json_is_array($group_json)){
            $group_array = json_decode($group_json, true);
            foreach ($group_array as $key => $action) {
                $action_code = $action['resource_group_id'];
                $codes .= ",'".$action_code."'";
            }
            // array_walk($group_array, function($action) use(&$codes){
            //     $action_code = $action['resource_group_id'];
            //     $codes .= ",'".$action_code."'";
            // });   
        }
        else{
            $action = json_decode($group_json, true);
            $action_code = $action['resource_group_id'];
            $codes .= ",'".$action_code."'";
        }
        $sql_select = "select resource_group_id from T_INVENTORY_RESOURCE_GROUP 
            where resource_group_id in($codes) and system_id not in('$system_id');";     
        $list = Tools::get_query_result($sql_select);
        return (count($list) > 0)?true:false;        
    }
    //增加新作业
    public function add_group()
    {
        $json = Tools::request("data");
        $system_id = Tools::get_system_id();
        
        // $json = '{"editing":false,"resource_group_id":"999","resource_group_name":"u999","note":"gggg"}';
        // $system_id = 'inventory';

        $already_exists = $this->check_group_id_exists($json, $system_id);
        if($already_exists){
            $foo_json = Tools::set_result_json('failed', '在其它系统中可能存在相同的要素类编码，请尽量使用不同的编码。');
        }
        else{
            $flag = $this->save_group_to_db($json, $system_id);
            if($flag){
                $foo_json = Tools::set_result_json('ok', '添加新要素类成功！');
            }
            else{
                $foo_json = Tools::set_result_json('failed', '添加要素类时出现异常！');
            }
        }
        echo $foo_json;
    }
    function save_group_to_db($group_json, $system_id)
    {
        $sql_replace = "";

        if(Tools::json_is_array($group_json)){

            $group_array = json_decode($group_json, true);

            foreach ($group_array as $key => $action) {
                $resource_group_id = $action['resource_group_id'];
                $resource_group_name = $action['resource_group_name'];
                $note = $action['note'];               
                $sql_replace .= 
                    "replace into T_INVENTORY_RESOURCE_GROUP
                    (resource_group_id, resource_group_name, system_id, note)
                    values('$resource_group_id', '$resource_group_name', '$system_id', '$note');"; 
            }
            // array_walk($group_array, function($action, $i) use(&$sql_replace){
            //     $resource_group_id = $action['resource_group_id'];
            //     $resource_group_name = $action['resource_group_name'];
            //     $note = $action['note'];               
            //     // $action_work_capability = $action['action_work_capability'];
            //     $sql_replace .= 
            //         "replace into T_INVENTORY_RESOURCE_GROUP
            //         (resource_group_id, resource_group_name, system_id, note)
            //         values('$resource_group_id', '$resource_group_name', '$system_id', '$note');";               
            // });
        }
        else{
            $action = json_decode($group_json, true);
            $resource_group_id = $action['resource_group_id'];
            $resource_group_name = $action['resource_group_name'];
            $note = $action['note'];               
            $sql_replace .= 
                "replace into T_INVENTORY_RESOURCE_GROUP
                (resource_group_id, resource_group_name, system_id, note)
                values('$resource_group_id', '$resource_group_name', '$system_id', '$note');";               
        }
        return Tools::trans_sql($sql_replace);        
    }
}
?>