<?php
require_once('tools.php');
	/**
	* 
	*/
class CostCaculateAction extends Action
{
    static public function get_product_final_cost($product_id = '', $system_id = '', $user_id = '')
    {
        $array_result = array();
        //计算单位作业成本
        $array_action_cost = self::action_final_cost($system_id, $user_id);
        // 获取每个产品各个作业的使用量
        $a_product_action_count = array();

        $sql_select_all_product_action_count
             = "select T1.product_id, T1.product_name, T1.note, ifnull(T2.action_count, 0) action_count , T2.action_code 
            from T_PRODUCT T1 left join T_PRODUCT_LINK_INFO T2  on T1.product_id = T2.product_id
              where T1.product_id = '$product_id' and T2.user_id = '$user_id';";
        $select_result_all_action_count = Tools::get_query_result($sql_select_all_product_action_count);

        $array_action_count = array();
        foreach ($select_result_all_action_count as $key => $row) {
            $action_code = $row['action_code'];
            $array_action_count[$action_code]['action_code'] = $action_code; 
            $array_action_count[$action_code]['action_count'] = $row['action_count'];
        }
        // array_walk($select_result_all_action_count, function($row, $key) use(&$array_action_count){
        //     $action_code = $row['action_code'];
        //     $array_action_count[$action_code]['action_code'] = $action_code; 
        //     $array_action_count[$action_code]['action_count'] = $row['action_count'];
        // });
        foreach ($array_action_cost as $key => $item) {
            $each_work_fee = $item['each_work_fee'];
            $action_code =  $item['action_code'];
            $unit =  $item['unit'];
            $action_cost = 0;
            $action_count = 0;
            if(array_key_exists($action_code, $array_action_count))
            {
                $action_count = $array_action_count[$action_code]['action_count'];
                $action_cost = $each_work_fee * $action_count;
            }
            $array_result[] = array('action_code' => $action_code
                                    , 'action_name' => $item['action_name']
                                    , 'each_work_fee' => ($each_work_fee)
                                    , 'action_cost' => ($action_cost)
                                    , 'unit' => ($unit)
                                    , 'action_count' => $action_count);
        }
        // $array_result = array_map(function($item) use($array_action_count){
        //     $each_work_fee = $item['each_work_fee'];
        //     $action_code =  $item['action_code'];
        //     $unit =  $item['unit'];
        //     $action_cost = 0;
        //     $action_count = 0;
        //     if(array_key_exists($action_code, $array_action_count))
        //     {
        //         $action_count = $array_action_count[$action_code]['action_count'];
        //         $action_cost = $each_work_fee * $action_count;
        //     }
        //     return array('action_code' => $action_code
        //                             , 'action_name' => $item['action_name']
        //                             , 'each_work_fee' => ($each_work_fee)
        //                             , 'action_cost' => ($action_cost)
        //                             , 'unit' => ($unit)
        //                             , 'action_count' => $action_count);

        // }, $array_action_cost);        
        return $array_result;
    }    
    static public function action_final_cost($system_id = '', $user_data_set = '')
    {
        $array_distribution = self::prepare_resource_distribution_info($system_id, $user_data_set);
        $array_resource_info = self::prepare_resource_info($system_id, $user_data_set);
         //计算各个资源的总数，之后才能计算每个资源在各个作业上分配的比例
         $array_total_each_resource_usage = self::resource_total_usage($array_distribution);
         //把每个资源的实际耗费的费用放到数组$array_distribution中
        $array_each_resource_fee = self::each_resource_fee($array_distribution, $array_resource_info, $array_total_each_resource_usage);     
        $array_action_fee = self::each_action_resource_group_fee($array_each_resource_fee, $array_resource_info);
        // return $array_action_fee;
        $list_all_inventory_resource_groups
             = self::get_all_system_inventory_resource_groups_with_para($system_id);

        $all_action_info = self::get_all_action_info($system_id, $user_data_set);
        $result = self::action_properties_fee($all_action_info, $list_all_inventory_resource_groups, $array_action_fee);

        return $result;    
    }    
    static public function get_all_system_inventory_resource_groups_with_para($system_id)
    {
        $sql_select_all_inventory_resource_groups = 
            "select resource_group_id, resource_group_name, system_id from T_INVENTORY_RESOURCE_GROUP where system_id = '$system_id';";
        $list_all_inventory_resource_groups = 
            Tools::get_query_result($sql_select_all_inventory_resource_groups);
        return $list_all_inventory_resource_groups;        
    }
    static public function get_all_system_inventory_resource_groups()
    {
        $system_id = Tools::get_system_id();
        $sql_select_all_inventory_resource_groups = 
            "select resource_group_id, resource_group_name, system_id from T_INVENTORY_RESOURCE_GROUP where system_id = '$system_id';";
        $list_all_inventory_resource_groups = 
            Tools::get_query_result($sql_select_all_inventory_resource_groups);
        return $list_all_inventory_resource_groups;
    }
        /**
    +----------------------------------------------------------
    * 内部函数
    +----------------------------------------------------------
    */

    static public function get_all_action_info($system_id = '', $user_data_set = '')
    {
        $sql_select 
            = "select T1.action_code, T1.action_name, T1.action_note, T1.system_id, T1.unit, ifnull(T2.action_work_capability, 0) action_work_capability 
            from  T_ACTION_INFO T1 left join T_ACTION_LINK_WORK_CAPABILITY T2 on T1.action_code = T2.action_code and T2.user_id = '$user_data_set'
             where T1.system_id = '$system_id'";
        return Tools::get_query_result($sql_select);        
    }    
    static public function action_properties_fee($all_action_info, $list_all_inventory_resource_groups, $action_fee_array)
    {
        $result = array();
        if (count($all_action_info) > 0) {
            for($i = 0; $i< count($all_action_info); $i++){
                $row = $all_action_info[$i];
                $action_code = $row['action_code'];

                $result[$action_code] = array('action_code' => $action_code
                                , 'action_name' => $row['action_name']
                                , 'unit' => $row['unit']
                                , 'action_work_capability' => $row['action_work_capability']);
                $action_fee_index = -1;
                foreach ($action_fee_array as $key => $value) {
                    if($value['action_code'] == $action_code){
                        $action_fee_index = $key;
                        break;
                    }
                }
                $total_inventory_fee = 0;
                foreach ($list_all_inventory_resource_groups as $key => $value){
                    $resource_group_id = $value['resource_group_id'];
                    if($action_fee_index >= 0 && array_key_exists($resource_group_id, $action_fee_array[$action_fee_index])) {
                        // $result[$action_code][$resource_group_id.'_fee'] = number_format($action_fee_array[$action_fee_index][$resource_group_id] + 0, 2);
                        $result[$action_code][$resource_group_id.'_fee'] = $action_fee_array[$action_fee_index][$resource_group_id] + 0;
                    }
                    else{
                        $result[$action_code][$resource_group_id.'_fee'] = 0;
                    }
                    $total_inventory_fee += $result[$action_code][$resource_group_id.'_fee'];
                }
                // $result[$action_code]['action_total_fee'] = number_format($total_inventory_fee, 2);
                $result[$action_code]['action_total_fee'] = $total_inventory_fee;
                if($result[$action_code]['action_work_capability'] > 0) {
                    // $result[$action_code]['each_work_fee'] = number_format($total_inventory_fee / $result[$action_code]['action_work_capability'], 2);
                    $result[$action_code]['each_work_fee'] = $total_inventory_fee / $result[$action_code]['action_work_capability'];
                }
                else {
                    $result[$action_code]['each_work_fee'] = 0;
                }
            }
        }
        return $result;        
    }

    static public function resource_total_usage($array_distribution)
    {
         $array_total_each_resource_usage = array();
         
         foreach ($array_distribution as $key => $value) {
            $resource_id = $value['resource_id'];
            if(array_key_exists($resource_id, $array_total_each_resource_usage))
            {
                $array_total_each_resource_usage[$resource_id] += $value['usage'];
            }
            else
            {
                $array_total_each_resource_usage[$resource_id] = ($value['usage'] + 0);
            }
         }
         return $array_total_each_resource_usage;        
    }
    static public function each_resource_fee($array_distribution, $array_resource_info, $array_total_each_resource_usage)
    {
         foreach ($array_distribution as $key => $value) {
            $resource_id = $value['resource_id'];

            if(array_key_exists($resource_id, $array_total_each_resource_usage) && 
                $array_total_each_resource_usage[$resource_id] > 0 &&
                array_key_exists($resource_id, $array_resource_info) &&
                $array_resource_info[$resource_id]['resource_fee'] > 0)
            {
                $array_distribution[$key]['resource_fee'] = $array_resource_info[$resource_id]['resource_fee'] * $value['usage'] / $array_total_each_resource_usage[$resource_id];
            }
            else
            {
                $array_distribution[$key]['resource_fee'] = 0;
            }
         } 
         return $array_distribution;        
    }

    static public function each_action_resource_group_fee($array_distribution, $array_resource_info)
    {
        $result = array();
        foreach ($array_distribution as $key => $value){
            $resource_id = $value['resource_id'];
            if(array_key_exists($value['action_code'], $result)){
                //查找资源所在的资源组，并将费用加到该资源组的费用中
                if(array_key_exists($resource_id, $array_resource_info)){
                    $group_id = $array_resource_info[$resource_id]['resource_group'];
                    $result[$value['action_code']][$group_id] += $value['resource_fee'];
                }
            }
            else{
                $result[$value['action_code']]['action_code'] = $value['action_code'];
                if(array_key_exists($resource_id, $array_resource_info)){
                    $group_id = $array_resource_info[$resource_id]['resource_group'];
                    $result[$value['action_code']][$group_id] = $value['resource_fee'];
                }
            }
        }        
        return $result;
         // 返回数据的格式
        // [{"action_code":"1000","10000":25},{"action_code":"2000","20000":200,"10000":75},{"action_code":"3000","30000":300}]

    } 
    static public function prepare_resource_info($system_id = '', $user_data_set = '')
    {
        //每种投入要素的费用
        //区分系统和用户
        $sql_resource_info = "select T1.resource_id, T2.resource_group, ifnull(T3.resource_fee, 0) resource_fee
                                from T_INVENTORY_RESOURCE T1,T_INVENTORY_LINK_ITEM T2, T_INVENTORY_RESOURCE_GROUP T3
                                left join T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE T3 on T1.resource_id = T3.resource_id and T3.user_id = '$user_data_set'
                                 where T1.resource_id = T2.resource_id and T2.resource_group = T3.resource_group_id and T3.system_id = '$system_id';";        
        $list2 = Tools::get_query_result($sql_resource_info);

        $array_resource_info = array();
        foreach ($list2 as $key => $value) {
            $array_resource_info[$value['resource_id']] 
                    = array('resource_id' => $value['resource_id'],
                            'resource_group' => $value['resource_group'],
                            'resource_fee' => $value['resource_fee']);
         }
        return $array_resource_info;         
    }   
    static public function prepare_resource_distribution_info($system_id = '', $user_data_set = '')
    {
        //获取每个作业的每个投入要素的分配量
        $sql_select_action_code_and_resource_id_and_useage = 
                "SELECT  T1.action_code, T1.resource_id, T1.usage FROM T_INVENTORY_RESOURCE_ACTION_USAGE T1 , T_ACTION_INFO T2 
                where T1.action_code = T2.action_code and T2.system_id = '$system_id' and T1.user_id = '$user_data_set';";
        // echo $sql_select_action_code_and_resource_id_and_useage;return;
        // return $sql_select_action_code_and_resource_id_and_useage;
        $list_action_code_and_resource_id_and_useage = 
                Tools::get_query_result($sql_select_action_code_and_resource_id_and_useage);

        $array_distribution = array();
        foreach ($list_action_code_and_resource_id_and_useage as $index => $value) {
            $key = $value['resource_id'].'_'.$value['action_code'];
            $array_distribution[$key] 
                = array('resource_id' => $value['resource_id'],
                        'action_code' => $value['action_code'],
                        'usage' => $value['usage']);
        }
        // var_dump($array_distribution);echo '<br>';
        return $array_distribution;

        // $para = array('resource_info' => $array_resource_info, 'resource_distribution' => $array_distribution);
        // return $para;
        // 数据结构
        // $Data['resource_info']['resource_id1'] = array('resource_id1' + 'resource_group' + 'tatal_resource_id1_fee')
        // $Data['resource_info']['resource_id2'] = array('resource_id2' + 'resource_group' + 'tatal_resource_id2_fee')
        // $Data['resource_info']['resource_id3'] = array('resource_id3' + 'resource_group' + 'tatal_resource_id3_fee')
        // ...
        // $Data['resource_distribution']['resource_id1'] = array('action_id' + 'resource_id1' + 'percent')
        // $Data['resource_distribution']['resource_id2'] = array('action_id' + 'resource_id1' + 'percent')
        // ...
        // 返回数据结构
        // ['action_id'] = 'action_id' + 'resource_group_id' + 'fee'
    }

}

?>