<?php

require_once('tools.php');
class LogisAAction extends Action
{
    
    // 作业设定
    public function ActionSetting_admin()
    {
        $this->display();
    }
    public function ActionSetting_user()
    {
        $this->display();
    }
    public function list_action_final_cost_index_viewer()
    {
        $user_id = Tools::request("user_id");
        $this->assign('user_id', $user_id);
        // $user_name = Tools::request('user_name');
        $sql = "SELECT ACCOUNT, REMARK FROM THINK_USER  where ACCOUNT = '$user_id';";
        $list_user = Tools::get_query_result($sql);
        if(count($list_user) > 0){
            $this->assign('user_name', $list_user[0]['REMARK']);
        }
        
        $a_items = array();

        $list_all_inventory_resource_groups
             = CostCaculateAction::get_all_system_inventory_resource_groups();
        for($i = 0; $i < count($list_all_inventory_resource_groups); $i++)
        {
            $group = $list_all_inventory_resource_groups[$i];

            $a_items[$i]['field'] = $group['resource_group_id'].'_fee';
            $a_items[$i]['value'] = $group['resource_group_name'];
        }

        $this->assign('list', $a_items);
        $this->display();
    }
    public function export_result_excel_viewer()
    {
        $system_id = Tools::get_system_id();
        $user_id = Tools::request('user_id');
        $this->export_result_excel($system_id, $user_id);           
    }
    public function export_result_excel_user()
    {
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();
        $this->export_result_excel($system_id, $user_id);        
    }
    public function export_result_excel($system_id, $user_id)
    {
        $array_result = CostCaculateAction::action_final_cost($system_id, $user_id);

        $list_all_inventory_resource_groups = CostCaculateAction::get_all_system_inventory_resource_groups();
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();
        //设置列标题
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '作业编码')
                                            ->setCellValue('B1','作业名称')
                                            ->setCellValue('c1', '总作业成本')
                                            ->setCellValue('d1', '月处理量')
                                            ->setCellValue('e1','单位处理成本');
        $a_new_column_num = Tools::number_to_ABC_map('f', count($list_all_inventory_resource_groups));
        $a_column_resource_group_id_map = array();
        for($i = 0; $i < count($a_new_column_num); $i++)
        {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($a_new_column_num[$i].'1', $list_all_inventory_resource_groups[$i]['resource_group_name']);
            $a_column_resource_group_id_map[$a_new_column_num[$i]] = $list_all_inventory_resource_groups[$i]['resource_group_id'];
        }
        $a_all_column_num = Tools::number_to_ABC_map('a', 5 + count($list_all_inventory_resource_groups));

        //输入数据
        $i = -1;
        foreach ($array_result as $key => $value) {
            $i ++;
            for($j = 0; $j < count($a_all_column_num); $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 2;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_code']);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_name']);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_total_fee']);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_work_capability']);
                        break;
                    case 'E':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['each_work_fee']);
                        break;
                    default:
                        $group_id = $a_column_resource_group_id_map[$column_index];
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value[$group_id.'_fee']);
                        break;
                }
            }
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
    public function action_final_cost_company_system_index_user()
    {
        $user_id = Tools::get_user_data_set();
        $this->assign('user_id', $user_id);

        $this->display();
    }
    public function action_final_cost_company_system_index_viewer()
    {
        $user_id = Tools::request("user_id");
        $user_name = Tools::request("user_name");
        $this->assign('user_id', $user_id);
        $this->assign('user_name', $user_name);

        $this->display();
    }
    public function list_action_final_cost_index()
    {
        $a_items = array();

        // 动态添加作业列
        $list_all_inventory_resource_groups 
            = CostCaculateAction::get_all_system_inventory_resource_groups();
        for($i = 0; $i < count($list_all_inventory_resource_groups); $i++){
            $group = $list_all_inventory_resource_groups[$i];

            $a_items[$i]['field'] = $group['resource_group_id'].'_fee';
            $a_items[$i]['value'] = $group['resource_group_name'];
        }

        $this->assign('list', $a_items);
        $this->display();
    }
    public function export_excel_company_action_cost_viewer()
    {
        $user_id = Tools::request("user_id");
        $array_action_cost_list = $this->get_chart_data_company_action_cost($user_id);
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();
        //设置列标题
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '系统名称')
                                            ->setCellValue('B1','总作业成本')
                                            ->setCellValue('c1', '总处理量')
                                            ->setCellValue('d1', '平均作业成本');


        $a_all_column_num = Tools::number_to_ABC_map('a', count($array_action_cost_list));

        //输入数据
        $i = -1;
        foreach ($array_action_cost_list as $key => $value) {
            $i ++;
            for($j = 0; $j < count($a_all_column_num); $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 2;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value['system_name']);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value['total_action_cost']);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value['total_action_work_capability']);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value['each_action_cost']);
                        break;
                }
            }
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;        
    }
    public function json_company_action_final_cost_for_single_user()
    {
        $user_id = Tools::request("user_id");
        // $user_id = 'user1';
        $array_action_cost_list = $this->get_chart_data_company_action_cost($user_id);
        echo json_encode(array_values($array_action_cost_list));            
    }
    function get_chart_data_company_action_cost($user_id)
    {
        $system_list_array = Tools::system_list_array();
        $array_action_cost_list = array();
        foreach ($system_list_array as $key => $system) {
            $array_system_action_cost_list 
                = CostCaculateAction::action_final_cost($system['id'], $user_id);
            $total_action_cost = 0;
            $total_action_work_capability = 0;
            foreach ($array_system_action_cost_list as $key => $value) {
                $total_action_work_capability += Tools::format_number_to_fload($value['action_work_capability']);
                $total_action_cost += Tools::format_number_to_fload($value['action_total_fee']);
            }
            $each_action_cost = 0;
            if($total_action_work_capability > 0){
                $each_action_cost = $total_action_cost/$total_action_work_capability;
            }
            $array_action_cost_list[] 
                = array('system_name' => $system['text'],
                        'total_action_cost' => number_format($total_action_cost, 2),
                        'total_action_work_capability' => number_format($total_action_work_capability,2),
                        'each_action_cost' => number_format($each_action_cost, 2));

        } 
        return $array_action_cost_list;       
    }
    public function json_all_action_final_cost_for_single_user()
    {
        $user_id = Tools::request("user_id");
        // $user_id = "user1";
        $system_id = Tools::get_system_id();
        $array_action_cost_list = CostCaculateAction::action_final_cost($system_id, $user_id);
        $result = array();
        foreach ($array_action_cost_list as $k => $action_cost) {
            foreach ($action_cost as $key => $value) {
                if(strpos($key, '_fee')){
                    $action_cost[$key] = number_format($value, 2);
                }
            }
            $result[] = $action_cost;
        }
        // $result = array_map(function($action_cost){
        //     foreach ($action_cost as $key => $value) {
        //         if(strpos($key, '_fee')){
        //             $action_cost[$key] = number_format($value, 2);
        //         }
        //     }
        //     return $action_cost;
        // }, $array_action_cost_list);

        echo json_encode(array_values($result));        
    }
    public function list_action_final_cost_list()
    {
        $user_id = Tools::request("user_id");
        if(empty($user_id)){
            $user_id = Tools::get_user_data_set();
        }
        $system_id = Tools::get_system_id();
        $array_action_cost_list = CostCaculateAction::action_final_cost($system_id, $user_id);
        //对要返回的结果进行必要的格式化
        $result = array();
        foreach ($array_action_cost_list as $k => $action_cost) {
            foreach ($action_cost as $key => $value) {
                if(strpos($key, '_fee')){
                    $action_cost[$key] = number_format($value, 2);
                }
            }
            $result[] = $action_cost;
        }
        // $result = array_map(function($action_cost){
        //     foreach ($action_cost as $key => $value) {
        //         if(strpos($key, '_fee')){
        //             $action_cost[$key] = number_format($value, 2);
        //         }
        //     }
        //     return $action_cost;
        // }, $array_action_cost_list);
        echo json_encode(array_values($result));
    }
    public function user_get_chart_data_action_cost()
    {
        $user_id = Tools::request("user_id");
        if(empty($user_id))
        {
            $user_id = Tools::get_user_data_set();
        }
        $system_id = Tools::get_system_id();


        echo $this->get_chart_data_action_cost($user_id, $system_id);        
    }
    public function viewer_get_chart_data_company_action_cost()
    {
        $user_id = Tools::request("user_id");
        $user_id = 'user1';
        $array_action_cost_list = $this->get_chart_data_company_action_cost($user_id);
        $pies_data_array = array();
        foreach ($array_action_cost_list as $key => $item) {
            $pies_data_array[] = "['".$item['system_name']."',".$item['total_action_cost']."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['system_name']."',".$item['total_action_cost']."]";
        // }, $array_action_cost_list);

        echo  '['.implode(',',$pies_data_array).']';return;

        // $all_action_total_fee = 0;
        // foreach ($array_action_cost_list as $key => $value) {
        //     $all_action_total_fee += $value['total_action_cost'];
        // }
        // $pies = array();
        // foreach ($array_action_cost_list as $key => $item) {
        //     $action_total_fee = $item['total_action_cost'];
        //     $percent = 0;
        //     if($action_total_fee > 0)
        //     {
        //         $percent = number_format(100 * $action_total_fee / $all_action_total_fee, 2);
        //     }
        //     $pies[] = array('label' => $item['system_name'].'('.$percent.'%)', 
        //                     'value' => Tools::format_number_to_fload($item['total_action_cost']));
        // }
        // $pies_json = json_encode($pies);

        // $colors = Tools::randomColors(count($array_action_cost_list));
        // echo '{
        //   "elements":[
        //     {
        //       "type":      "pie",
        //       "colours":   '
        //       .json_encode($colors).',
        //       "alpha":     0.6,
        //       "border":    2,
        //       "start-angle": 35,
        //       "values" :   '
        //       .$pies_json.'
        //     }
        //   ]
        // }';        
    }
    public function viewer_get_chart_data_action_cost()
    {
        $user_id = Tools::request("user_id");
        // $user_id = 'user1';
        if(empty($user_id))
        {
            echo '[]';return;
        }
        $system_id = Tools::get_system_id();

        // $system_id = 'inventory';

        echo $this->get_chart_data_action_cost($user_id, $system_id);        
    }


    public function save_data()
    {
        $json_str = Tools::request("data");
        if(empty($json_str)){
            $foo_json = Tools::set_result_json('failed', '没有数据被保存！');
        }
        else{
            $user_id = Tools::get_user_data_set();
            $system_id = Tools::get_system_id();
            $flag = $this->save_action_cost_history($json_str, $user_id, $system_id);
            if($flag){
                $foo_json = Tools::set_result_json('ok', '数据已保存, 请在作业成本历史页面查看。');
            }
            else{
                $foo_json = Tools::set_result_json('failed', '插入数据时出现异常');
            }
        }
        echo $foo_json;  
    }

    public function remove_action()
    {
        $json = Tools::request_json("data");
        $flag = $this->remove_action_from_database($json);
        echo $flag ? 'ok':'failed';
    }

    // 更新个人设定的作业处理量
    // 需要检查已分配在产品上的处理量综合是否已经超过新设定
    // 如果前者大于后者，需要用户首先修改已分配的处理量
    public function update_action_work_capability()
    {
        $json = Tools::request("data");
        // $user_id = $_SESSION['acount'];
        $user_id = Tools::get_user_data_set();
        $actions_array_invalidation 
            = $this->check_if_work_capability_too_small($json, $user_id);

        if(!Tools::all_array_member_is_NULL($actions_array_invalidation)){
            $foo_json = Tools::set_result_json('failed', '新设定的作业处理量小于产品中已分配作业处理量的总和，请重新设定！');
        }
        else{
            $r = $this->update_action_work_capability_to_database($json, $user_id);
            if($r){
                $foo_json = Tools::set_result_json('ok', '数据保存成功！');
            }
            else{
                $foo_json = Tools::set_result_json('failed', '数据更新失败！');
            }
        }
        echo $foo_json;  
    }
    //增加新作业，管理员操作
    public function add_action()
    {
        $json = Tools::request("data");
        $system_id = Tools::get_system_id();
        if($this->check_action_code_exists($json, $system_id)){
            $foo_json = Tools::set_result_json('failed', '在其它系统中可能存在相同的作业编码，请尽量使用不同的编码。');
        }
        else{
            $flag = $this->add_action_to_database($json, $system_id);
            if($flag){
                $foo_json = Tools::set_result_json('ok', '添加新作业成功！');
            }
            else{
                $foo_json = Tools::set_result_json('failed', '添加作业时出现异常！');
            }
        }
        echo $foo_json;
    }


    public function system_list()
    {
        $result = Tools::system_list_array();
        $foo_json = json_encode($result);
        echo $foo_json;
    }

    //返回作业列表，只包含基本属性
    public function actions_list_admin()
    {
        $system_id = Tools::get_system_id();
        $sql_select = "select action_code, action_name, action_note, system_id, unit  
                    from  T_ACTION_INFO where system_id = '$system_id' order by action_code;";
        $select_result = Tools::get_query_result($sql_select);
        $foo_json = json_encode($select_result);
        echo $foo_json;
    }
    //设置作业的处理量页面所需的作业列表，需要区分用户
    public function actions_list_system()
    {
        $system_id = Tools::get_system_id();
        $user_data_set = $_SESSION['user_data_set'];

        $sql_select = "select T1.action_code, T1.action_name, T1.action_note, T1.system_id, T1.unit, ifnull(T2.action_work_capability, 0) action_work_capability 
                    from  T_ACTION_INFO T1 left join T_ACTION_LINK_WORK_CAPABILITY T2 on T1.action_code = T2.action_code and T2.user_id = '$user_data_set'
                     where T1.system_id = '$system_id'";
        $select_result = Tools::get_query_result($sql_select);
        $foo_json = json_encode($select_result);
        echo $foo_json;
    }
        /**
    +----------------------------------------------------------
    * 数据计算输出
    +----------------------------------------------------------
    */
    public function get_chart_data_action_cost($user_id, $system_id)
    {
        if(empty($system_id)){
            $system_list_array = Tools::system_list_array();
        }
        else{
            $system_list_array[] = Tools::get_system_info_by_id(Tools::system_list_array(), $system_id);
        }

        $array_action_cost_list = array();
        foreach ($system_list_array as $key => $system) {
            $array_system_action_cost_list 
                = CostCaculateAction::action_final_cost($system['id'], $user_id);

            foreach($array_system_action_cost_list as $key => $item) {
                    $item['system_id'] = $system['text'];
                    $array_new_system_action_cost_list[] = $item;
            }
            // $array_new_system_action_cost_list
            //     = array_map(function($item) use($system){
            //         $item['system_id'] = $system['text'];
            //         return $item;
            //     }, $array_system_action_cost_list);
            $array_action_cost_list = array_merge($array_action_cost_list, $array_new_system_action_cost_list);
        }
        foreach ($array_action_cost_list as $key => $item) {
            $pies_data_array[] = "['".$item['action_name']."',".$item['action_total_fee']."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['action_name']."',".$item['action_total_fee']."]";
        // }, $array_action_cost_list);

        return '['.implode(',',$pies_data_array).']';
        // $all_action_total_fee = 0;
        // foreach ($array_action_cost_list as $key => $item) {
        //     $all_action_total_fee += $item['action_total_fee'];
        // }

        // $pies = array();
        // foreach ($array_action_cost_list as $key => $item) {
        //     $action_total_fee = $item['action_total_fee'];
        //     $percent = 0;
        //     if($action_total_fee > 0)
        //     {
        //         $percent = number_format(100 * $action_total_fee / $all_action_total_fee, 2);
        //     }
        //     $pies[] = array('label' => $item['action_name'].'('.$percent.'%)', 
        //                     'value' => ($item['action_total_fee'] + 0));
        // }
        // $pies_json = json_encode($pies);

        // $colors = Tools::randomColors(count($array_action_cost_list));
        // return '{
        //   "elements":[
        //     {
        //       "type":      "pie",
        //       "colours":   '
        //       .json_encode($colors).',
        //       "alpha":     0.6,
        //       "border":    2,
        //       "start-angle": 35,
        //       "values" :   '
        //       .$pies_json.'
        //     }
        //   ]
        // }';

    }    
    function check_action_code_exists($actions_json, $system_id)
    {
        $codes = "''";
        if(Tools::json_is_array($actions_json)){
            $actions_array = json_decode($actions_json, true);
            foreach ($actions_array as $key => $action) {
                $action_code = $action['action_code'];
                $codes .= ",'".$action_code."'";
            }
            // array_walk($actions_array, function($action) use(&$codes){
            //     $action_code = $action['action_code'];
            //     $codes .= ",'".$action_code."'";
            // });   
        }
        else{
            $action = json_decode($actions_json, true);
            $action_code = $action['action_code'];
            $codes .= ",'".$action_code."'";
        }
        $sql_select = "select action_code from T_ACTION_INFO 
            where action_code in($codes) and system_id not in('$system_id');";     
        $list = Tools::get_query_result($sql_select);
        return (count($list) > 0)?true:false;
    }
    public function add_action_to_database($actions_json, $system_id)
    {
        $sql_replace = "";
        if(Tools::json_is_array($actions_json)){
            $actions_array = json_decode($actions_json, true);
            foreach ($actions_array as $key => $action) {
                $action_code = $action['action_code'];
                $action_name = $action['action_name'];
                $action_note = $action['action_note'];               
                $unit = $action['unit'];               
                $sql_replace .= "replace into T_ACTION_INFO
                                (action_code, action_name, action_note, system_id, unit)
                                values
                                ('$action_code', '$action_name', '$action_note', '$system_id', '$unit');";               
            }
            // array_walk($actions_array, function($action, $i) use(&$sql_replace){
            //     $action_code = $action['action_code'];
            //     $action_name = $action['action_name'];
            //     $action_note = $action['action_note'];               
            //     $unit = $action['unit'];               
            //     // $action_work_capability = $action['action_work_capability'];
            //     $sql_replace .= "replace into T_ACTION_INFO
            //                     (action_code, action_name, action_note, system_id, unit)
            //                     values
            //                     ('$action_code', '$action_name', '$action_note', '$system_id', '$unit');";               
            // });
        }
        else{
            $action = json_decode($actions_json, true);
            $action_code = $action['action_code'];
            $action_name = $action['action_name'];
            $action_note = $action['action_note'];               
            $unit = $action['unit'];               
            $sql_replace .= "replace into T_ACTION_INFO
                            (action_code, action_name, action_note, system_id, unit)
                            values
                            ('$action_code', '$action_name', '$action_note', '$system_id', '$unit');";               

        }
        return Tools::trans_sql($sql_replace);
    }
    public function update_action_work_capability_to_database($actions_json, $user_id)
    {
        $sql_replace = "";
        if(Tools::json_is_array($actions_json)){
            $actions_array = json_decode($actions_json, true);
            foreach ($actions_array as $key => $action) {
                $action_code = $action['action_code'];
                $action_work_capability = $action['action_work_capability'];
                $sql_replace .= "replace into T_ACTION_LINK_WORK_CAPABILITY
                                (action_code, user_id, action_work_capability)
                                values
                                ('$action_code', '$user_id', $action_work_capability);";               
            }
            // array_walk($actions_array, function($action, $i) use($user_id, &$sql_replace){
            //     $action_code = $action['action_code'];
            //     $action_work_capability = $action['action_work_capability'];
            //     $sql_replace .= "replace into T_ACTION_LINK_WORK_CAPABILITY
            //                     (action_code, user_id, action_work_capability)
            //                     values
            //                     ('$action_code', '$user_id', $action_work_capability);";               

            // });
        }
        else{
            $action = json_decode($actions_json, true);
            $action_code = $action['action_code'];
            $action_work_capability = $action['action_work_capability'];
            $sql_replace .= "replace into T_ACTION_LINK_WORK_CAPABILITY
                            (action_code, user_id, action_work_capability)
                            values
                            ('$action_code', '$user_id', $action_work_capability);";               

        }
        return Tools::trans_sql($sql_replace);
    }
    public function check_if_work_capability_too_small($actions_json, $user_id)
    {
        //如果已分配的作业量大于新设置的总作业量，返回作业的编码
        $codes_array = array();
        if(Tools::json_is_array($actions_json)){
            $actions_array = json_decode($actions_json, true);
            $codes_array = array();
            foreach ($actions_array as $key => $action) {
                $action_code = $action['action_code'];
                $action_work_capability = $action['action_work_capability'];
                $sql_select = "select ifnull(sum(action_count),0) sum from T_PRODUCT_LINK_INFO where action_code = '$action_code' 
                                and user_id = '$user_id';";
                $list = Tools::get_query_result($sql_select);
                //说明新设定的量小于已分配的量
                if((count($list) > 0) && $list[0]['sum'] > $action_work_capability){
                    $codes_array[] = $action_code;
                }
            }
            // $codes_array = array_map(function($action) use($user_id){
            //     $action_code = $action['action_code'];
            //     $action_work_capability = $action['action_work_capability'];
            //     $sql_select = "select ifnull(sum(action_count),0) sum from T_PRODUCT_LINK_INFO where action_code = '$action_code' 
            //                     and user_id = '$user_id';";
            //     $list = Tools::get_query_result($sql_select);
            //     //说明新设定的量小于已分配的量
            //     if((count($list) > 0) && $list[0]['sum'] > $action_work_capability){
            //         return $action_code;
            //     }
            // }, $actions_array);
        }
        else{
            $action = json_decode($actions_json, true);
            $action_code = $action['action_code'];
            $action_work_capability = $action['action_work_capability'];
            $sql_select = "select ifnull(sum(action_count),0) sum from T_PRODUCT_LINK_INFO where action_code = '$action_code' 
                            and user_id = '$user_id';";
            $list = Tools::get_query_result($sql_select);
            //说明新设定的量小于已分配的量
            if((count($list) > 0) && $list[0]['sum'] > $action_work_capability){
                $codes_array[] = $action_code;
            }
        }
        return $codes_array;
    }
    public function remove_action_from_database($actions_array)
    {
        $codes = "''";
        foreach ($actions_array as $key => $action) {
            $action_code = $action['action_code'];
            $codes .= ",'".$action_code."'";
        }
        // array_walk($actions_array, function($action) use(&$codes){
        //     $action_code = $action['action_code'];
        //     $codes .= ",'".$action_code."'";
        // });
        $sql_delete = "delete from T_ACTION_INFO
                    where action_code in($codes);";
        $sql_delete .= "delete from T_ACTION_LINK_WORK_CAPABILITY where action_code in($codes);";
        $sql_delete .= "delete from T_INVENTORY_RESOURCE_ACTION_USAGE where action_code in($codes);";
        $sql_delete .= "delete from T_ACTION_LINK_HISTORY where action_code in($codes);";
        $sql_delete .= "delete from T_PRODUCT_LINK_INFO where action_code in($codes);";
        return Tools::trans_sql($sql_delete);
    }
    public function save_action_cost_history($action_costs_str, $user_id, $system_id)
    {
        //以日期作为键值，检查现有保存数据，则替换已有的
        date_default_timezone_set("Asia/Shanghai");
        $time = date("Y-m-d H:i:s");
        $data_id= 'report_'.date("YmdHis");
        $sql_replace = "replace into T_ACTION_COST_HISTORY(history_id, user_id,system_id, time_stamp, history)
             values('$data_id', '$user_id', '$system_id', '$time', '$action_costs_str');";
        return Tools::trans_sql($sql_replace);
    }



}
?>