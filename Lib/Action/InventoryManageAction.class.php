<?php

require_once('tools.php');
class InventoryManageAction extends Action
{

    // 根据传递的投入要素类型，显示该类型要素下的具体项，并对其进行管理
    public function inventory_resource_select_admin()
    {
        $group_id = $_GET['group_id'];
        $sql_select_inventory_resource_group = 
            "select resource_group_id, resource_group_name, system_id, note from T_INVENTORY_RESOURCE_GROUP where resource_group_id = '$group_id';";
        // echo $sql_select_all_inventory_resource_groups; return;
        $list_inventory_resource_group = Tools::get_query_result($sql_select_inventory_resource_group);
        if(count($list_inventory_resource_group) > 0)
        {
            $this->assign('group_note', $list_inventory_resource_group[0]['note']);
            $this->assign('group_name', $list_inventory_resource_group[0]['resource_group_name']);
        }
        $this->assign('group_id', $group_id);
        $this->display();
    }
    // 根据传递的投入要素类型，显示该类型要素下的具体项，并修改其费用
    public function inventory_resource_select_user()
    {
        $group_id = $_GET['group_id'];
        $sql_select_inventory_resource_group = 
            "select resource_group_id, resource_group_name, system_id, note from T_INVENTORY_RESOURCE_GROUP where resource_group_id = '$group_id';";
        // echo $sql_select_all_inventory_resource_groups; return;
        $list_inventory_resource_group = Tools::get_query_result($sql_select_inventory_resource_group);
        if(count($list_inventory_resource_group) > 0)
        {
            $this->assign('group_note', $list_inventory_resource_group[0]['note']);
            $this->assign('group_name', $list_inventory_resource_group[0]['resource_group_name']);
        }
        $this->assign('group_id', $group_id);
        $this->display();
    }
    //返回投入要素的基础资料和该要素的费用，区分用户
    public function inventory_resource_select_list_user()
    {
        $group_id = $_GET['group_id'];
        $user_data_set = $_SESSION['user_data_set'];
        $sql_select_get_resource_id_with_group_id = 
        "select T1.resource_id, T1.resource_name, T1.resource_note, ifnull(T3.resource_fee, 0) resource_fee
            from T_INVENTORY_RESOURCE T1,T_INVENTORY_LINK_ITEM T2 
            left join T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE T3 on T1.resource_id = T3.resource_id and T3.user_id = '$user_data_set'
             where T1.resource_id = T2.resource_id and T2.resource_group = '$group_id'";
        $select_result = Tools::get_query_result($sql_select_get_resource_id_with_group_id);
        $foo_json = json_encode($select_result);
        echo $foo_json;          
    }
    public function inventory_resource_select_list_admin()
    {
        $group_id = $_GET['group_id'];
        $sql_select_get_resource_id_with_group_id = 
        "select T1.resource_id, T1.resource_name, T1.resource_note 
                        from T_INVENTORY_RESOURCE T1,T_INVENTORY_LINK_ITEM T2
                         where T1.resource_id = T2.resource_id and T2.resource_group = '$group_id' order by T1.resource_id;";
        $select_result = Tools::get_query_result($sql_select_get_resource_id_with_group_id);
        $foo_json = json_encode($select_result);
        echo $foo_json;       
    }
    // 根据传递的投入要素类型，显示该类型要素下，具体项在各个作业中的投入
    public function inventory_resource_used_percent_index()
    {
        $group_id = $_GET['group_id'];
        $sql_select_inventory_resource_group = 
            "select resource_group_id, resource_group_name, system_id, note from T_INVENTORY_RESOURCE_GROUP where resource_group_id = '$group_id';";
        $M_inventory_resource_group = new Model();
        $list_inventory_resource_group = $M_inventory_resource_group->query($sql_select_inventory_resource_group);
        if(count($list_inventory_resource_group) > 0)
        {
            $this->assign('group_note', $list_inventory_resource_group[0]['note']);
            $this->assign('group_name', $list_inventory_resource_group[0]['resource_group_name']);
        }
        $this->assign('group_id', $group_id);
        $this->display();
    }
     public function actions_list_for_used_percent()
    {
        $resource_id = Tools::request('resource_id');
        if(!empty($resource_id)){
            // $result = array();
            require_once('class.LogisAction.php');
            $system_id = $_SESSION['system_id'];
            $user_data_set = $_SESSION['user_data_set'];
            
            $sql_select = "select T1.action_code, T1.action_name, T1.action_note, ifnull(T2.usage, 0) usage from  T_ACTION_INFO T1 
                            left join T_INVENTORY_RESOURCE_ACTION_USAGE T2 on T1.action_code = T2.action_code and T2.user_id = '$user_data_set' 
                            and T2.resource_id = '$resource_id' 
                            where T1.system_id = '$system_id';";
            $select_result = Tools::get_query_result($sql_select);
            $foo_json = json_encode($select_result);            
        }

        echo $foo_json;
    } 

  //更改单个投入要素具体项在各个作业中投入数量
    //接收的参数包括作业编码和要素具体项的编码
    public function update_specified_action_info_for_resource_used_percent_set()
    {
        $json = Tools::request("data");
        $user_id = Tools::get_user_data_set();
        $flag = $this->update_specified_action_info_for_resource_used_percent_set_to_database($json,$user_id);
        // echo $flag; return;
        echo $flag ? 'ok':'failed';
    }
    //接收一个要素编码的数组，将与这些编码相关的数据全部删除

    //删除投入要素，管理员操作
    public function remove_inventory_item()
    {
        $json = Tools::request("data");
        $flag = $this->remove_inventory_item_from_database($json);
        echo $flag ? 'ok':'failed';
    }

    //添加投入要素，管理员操作
    public function add_inventory_item()
    {
        $json = Tools::request("data");
        $flag = $this->add_inventory_item_to_database($json);
        // echo $flag;return;
        echo $flag ? 'ok':'failed';
    }
    public function update_inventory_item_fee()
    {
        $json = Tools::request("data");
        $user_id = Tools::get_user_data_set();
        $flag = $this->update_inventory_item_fee_to_database($json, $user_id);
        // echo $flag; return;
        echo $flag ? 'ok':'failed';
    }


    /**

    */

    public function update_inventory_item_fee_to_database($inventory_items_json, $user_id)
    {
        $sql_replace = "";
        if(Tools::json_is_array($inventory_items_json)){
            $inventory_items_array = json_decode($inventory_items_json, true);
            foreach ($inventory_items_array as $key => $inventory_item) {
                $resource_id = $inventory_item['resource_id'];
                $resource_fee = $inventory_item['resource_fee'];               
                $sql_replace .= 
                    "replace into T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE(resource_id, user_id, resource_fee)
                    values('$resource_id','$user_id',$resource_fee);";
            }
            // array_walk($inventory_items_array, function($inventory_item, $i) use(&$sql_replace, $user_id){
            //     $resource_id = $inventory_item['resource_id'];
            //     $resource_fee = $inventory_item['resource_fee'];               
            //     $sql_replace .= 
            //         "replace into T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE(resource_id, user_id, resource_fee)
            //         values('$resource_id','$user_id',$resource_fee);";
            // });
        }
        else{
            $inventory_item = json_decode($inventory_items_json, true);
            $resource_id = $inventory_item['resource_id'];
            $resource_fee = $inventory_item['resource_fee'];               
            $sql_replace .= 
                "replace into T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE(resource_id, user_id, resource_fee)
                values('$resource_id','$user_id',$resource_fee);";
        }
        // return $sql_replace;
        return Tools::trans_sql($sql_replace);
    }

    public function add_inventory_item_to_database($inventory_items_json)
    {
        $sql_replace = "";
        if(Tools::json_is_array($inventory_items_json)){

            $inventory_items_array = json_decode($inventory_items_json, true);
            foreach ($inventory_items_array as $key => $inventory_item) {
                $resource_id = $inventory_item['resource_id'];
                $resource_group = $inventory_item['resource_group'];
                $resource_name = $inventory_item['resource_name'];
                $resource_note = $inventory_item['resource_note'];               
                $sql_replace .= "replace into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note)
                                values('$resource_id','$resource_name','$resource_note');";
                $sql_replace .= "replace into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('$resource_id','$resource_group');";
            }
            // array_walk($inventory_items_array, function($inventory_item, $i) use(&$sql_replace){
            //     $resource_id = $inventory_item['resource_id'];
            //     $resource_group = $inventory_item['resource_group'];
            //     $resource_name = $inventory_item['resource_name'];
            //     $resource_note = $inventory_item['resource_note'];               
            //     $sql_replace .= "replace into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note)
            //                     values('$resource_id','$resource_name','$resource_note');";
            //     $sql_replace .= "replace into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('$resource_id','$resource_group');";
            // });
        }
        else{

            $inventory_item = json_decode($inventory_items_json, true);
            $resource_id = $inventory_item['resource_id'];
            $resource_group = $inventory_item['resource_group'];
            $resource_name = $inventory_item['resource_name'];
            $resource_note = $inventory_item['resource_note'];               
            $sql_replace .= "replace into T_INVENTORY_RESOURCE(resource_id, resource_name, resource_note)
                            values('$resource_id','$resource_name','$resource_note');";
            $sql_replace .= "replace into T_INVENTORY_LINK_ITEM(resource_id, resource_group) values('$resource_id','$resource_group');";

        }
        // return $sql_replace;
        return Tools::trans_sql($sql_replace);
    }
    public function get_sql_remove_inventory_item_data($resource_id_array)
    {
        $resource_ids = "''";
        // array_walk($resource_id_array, function($value, $key) use(&$resource_ids){
        //         $resource_ids .= ",'".$value."'";
        //     });
        foreach ($resource_id_array as $key => $value) {
                $resource_ids .= ",'".$value."'";
            }    
        $sql_delete = "delete from T_INVENTORY_RESOURCE where resource_id in($resource_ids);";
        $sql_delete .= "delete from T_INVENTORY_RESOURCE_ACTION_USAGE where resource_id in($resource_ids);";
        $sql_delete .= "delete from T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE where resource_id in($resource_ids);";
        $sql_delete .= "delete from T_INVENTORY_LINK_ITEM where resource_id in($resource_ids);";

        return $sql_delete;
    }
    public function remove_inventory_item_from_database($inventory_items_json)
    {
        if(Tools::json_is_array($inventory_items_json)){
            $inventory_items_array = json_decode($inventory_items_json, true);
            $resource_id_array = array_map(function($value){
                return $value['resource_id'];
            }, $inventory_items_array);
        }
        else{
            $inventory_item = json_decode($inventory_items_json, true);
            $resource_id_array[] = $inventory_item['resource_id'];
        }

        $sql_delete = $this->get_sql_remove_inventory_item_data($resource_id_array);
        return Tools::trans_sql($sql_delete);               
    }
    public function update_specified_action_info_for_resource_used_percent_set_to_database($actions_json, $user_data_set)
    {
        $sql_insert = "";
        if(Tools::json_is_array($actions_json)){
            $actions_array = json_decode($actions_json, true);
            foreach ($actions_array as $key => $action) {
                    $action_code = $action['action_code'];
                    $resource_id = $action['resource_id'];
                    $usage = $action['usage'];
                    $sql_insert .= "replace into T_INVENTORY_RESOURCE_ACTION_USAGE(resource_id, action_code, user_id, usage)
                                     values('$resource_id', '$action_code', '$user_data_set', $usage);";
            }
            // array_walk($actions_array, function($action, $i) use(&$sql_insert, $user_data_set){
            //         $action_code = $action['action_code'];
            //         $resource_id = $action['resource_id'];
            //         $usage = $action['usage'];
            //         $sql_insert .= "replace into T_INVENTORY_RESOURCE_ACTION_USAGE(resource_id, action_code, user_id, usage)
            //                          values('$resource_id', '$action_code', '$user_data_set', $usage);";
            // });
        }
        else{
            $action = json_decode($actions_json, true);
            $action_code = $action['action_code'];
            $resource_id = $action['resource_id'];
            $usage = $action['usage'];
            $sql_insert .= "replace into T_INVENTORY_RESOURCE_ACTION_USAGE(resource_id, action_code, user_id, usage)
                             values('$resource_id', '$action_code', '$user_data_set', $usage);";
        }
        // return $sql_insert;
        return Tools::trans_sql($sql_insert);
    }


}
?>