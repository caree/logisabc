<?php
require_once('CostCaculateAction.class.php');
require_once('tools.php');
class UnitTestAction extends Action
{
	public function test()
	{
		// $this->test_add_and_remove_action();
		// $this->test_action_final_cost();

		// $this->test_product_cost();
		// $this->test_db_json();
		// $this->test_int_parse();
		// $this->echo_json();
		// $this->test_is_array();
		$this->test_all_array_member_is_NULL();
	}
	public function test_all_array_member_is_NULL()
	{
		$a = array();
		if(Tools::all_array_member_is_NULL($a)){
			echo 'True ';
		}
		$a[] = '1001';
		if(Tools::all_array_member_is_NULL($a)){
			echo 'False ';
		}
		else{
			echo 'True ';
		}		
	}
	public function test_is_array()
	{
		$json_array = '[{"itemid":"EST-1","productid":"FI-SW-01","listprice":"16.50","unitcost":"10.00","status":"P","attr1":"Large"},{"itemid":"EST-10","productid":"K9-DL-01","listprice":"18.50","unitcost":"12.00","status":"P","attr1":"Spotted Adult Female"},{"itemid":"EST-11","productid":"RP-SN-01","listprice":"18.50","unitcost":"12.00","status":"P","attr1":"Venomless"}]';
		$json = '{"itemid":"EST-1","productid":"FI-SW-01"}';
		if($this->json_is_array($json_array)){
			echo 'Ture  ';
		}
		else{
			echo 'False  ';
		}
		if(!$this->json_is_array($json)){
			echo 'Ture  ';
		}
		else{
			echo 'False  ';
		}
	}
	public function json_is_array($json)
	{
		$is_array = json_decode($json);
		if(is_array($is_array)){
			return true;
		}	
		return false;
	}
	public function echo_json()
	{
		echo '{"total":"28","rows":[{"itemid":"EST-1","productid":"FI-SW-01","listprice":"16.50","unitcost":"10.00","status":"P","attr1":"Large"},{"itemid":"EST-10","productid":"K9-DL-01","listprice":"18.50","unitcost":"12.00","status":"P","attr1":"Spotted Adult Female"},{"itemid":"EST-11","productid":"RP-SN-01","listprice":"18.50","unitcost":"12.00","status":"P","attr1":"Venomless"},{"itemid":"EST-12","productid":"RP-SN-01","listprice":"18.50","unitcost":"12.00","status":"P","attr1":"Rattleless"},{"itemid":"EST-13","productid":"RP-LI-02","listprice":"18.50","unitcost":"12.00","status":"P","attr1":"Green Adult"},{"itemid":"EST-14","productid":"FL-DSH-01","listprice":"58.50","unitcost":"12.00","status":"P","attr1":"Tailless"},{"itemid":"EST-15","productid":"FL-DSH-01","listprice":"23.50","unitcost":"12.00","status":"P","attr1":"With tail"},{"itemid":"EST-16","productid":"FL-DLH-02","listprice":"93.50","unitcost":"12.00","status":"P","attr1":"Adult Female"},{"itemid":"EST-17","productid":"FL-DLH-02","listprice":"93.50","unitcost":"12.00","status":"P","attr1":"Adult Male"},{"itemid":"EST-18","productid":"AV-CB-01","listprice":"193.50","unitcost":"92.00","status":"P","attr1":"Adult Male"}]}';
	}
	public function test_int_parse()
	{
		if(Tools::format_number_to_fload("14,711.05") == 14711.05){
			echo "ok  ";
		}
		else{
			echo 'failed  ';
			var_dump($d);
		}
	}
	public function test_db_json()
	{
        $actions_array[] = array('action_code' => 'unit_test_action_code1',
                                 'action_name' => 'unit_test_action_name1', 
                                 'action_note' => 'unit_test_action_note1', 
                                 'system_id' => 'unit_test_action_system_id1', 
                                 'unit' => 'unit_test_action_unit1');
        $actions_array[] = array('action_code' => 'unit_test_action_code2',
                                 'action_name' => 'unit_test_action_name2', 
                                 'action_note' => 'unit_test_action_note2', 
                                 'system_id' => 'unit_test_action_system_id2', 
                                 'unit' => 'unit_test_action_unit2');
        $actions_array[] = array('action_code' => 'unit_test_action_code3',
                                 'action_name' => 'unit_test_action_name3', 
                                 'action_note' => 'unit_test_action_note3', 
                                 'system_id' => 'unit_test_action_system_id3', 
                                 'unit' => 'unit_test_action_unit3');		
        $json = json_encode($actions_array);
		$sql_insert = "insert into tb_json_db(history_id,history) values('h1','$json');";
		$r = Tools::trans_sql($sql_insert);
		if($r){
			echo '插入数据成功 <br>';
		}
		else{
			echo '插入数据失败 <br>';
			return;
		}
		$sql_select = "select history_id, history from tb_json_db;";
		$list = Tools::get_query_result($sql_select);
		if(count($list) > 0){
			echo '查询到数据,下面为查询到的数据：<br>';
			var_dump(json_decode($list[0]['history']));
			echo '数据将会被删除<br>';
			$sql_delete = "delete from tb_json_db;";
			$r_delete = Tools::trans_sql($sql_delete);
			if($r_delete){
				echo '数据删除成功<br>';
			}
			else{
				echo '数据删除失败<br>';
			}
		}
		else{
			echo '没有查询到数据<br>';
			return;
		}

	}
	public function test_product_cost()
	{
		$user_id = 'user1';
		$system_id = 'inventory';
		$product_id = '1001';		
		$result = CostCaculateAction::get_product_final_cost($product_id, $system_id, $user_id);
		var_dump($result);
	}
	public function test_change_finded_array()
	{
		$a['111_fee'] = 111;
		$a['222_fee'] = 222;
		$key = array_keys($a);
		foreach ($a as $key => $value) {
			if(stristr($key, '_fee')){
				$a[$key] = $value + 1;
			}
			else{
				$a[$key] = $value - 1;
			}
		}
		var_dump($a);		
	}
	public function test_action_final_cost()
	{
		$user_id = 'user1';
		$system_id = 'inventory';
        $array_distribution 
	        = CostCaculateAction::prepare_resource_distribution_info($system_id, $user_id);
		// var_dump($array_distribution);
        $array_resource_info = CostCaculateAction::prepare_resource_info($system_id, $user_id);
		// var_dump($array_resource_info);
        $array_total_each_resource_usage = CostCaculateAction::resource_total_usage($array_distribution);
        // var_dump($array_total_each_resource_usage);
        $array_each_resource_fee
	         = CostCaculateAction::each_resource_fee($array_distribution, $array_resource_info, $array_total_each_resource_usage);     
        // var_dump($array_each_resource_fee);
        $array_action_fee = CostCaculateAction::each_action_resource_group_fee($array_each_resource_fee, $array_resource_info);
        // var_dump($array_action_fee);
		var_dump(CostCaculateAction::action_final_cost($system_id, $user_id));
	}
	public function test_add_and_remove_action()
	{
        $LogisAAction = A('LogisA');
        $actions_array[] = array('action_code' => 'unit_test_action_code1',
                                 'action_name' => 'unit_test_action_name1', 
                                 'action_note' => 'unit_test_action_note1', 
                                 'system_id' => 'unit_test_action_system_id1', 
                                 'unit' => 'unit_test_action_unit1');
        $actions_array[] = array('action_code' => 'unit_test_action_code2',
                                 'action_name' => 'unit_test_action_name2', 
                                 'action_note' => 'unit_test_action_note2', 
                                 'system_id' => 'unit_test_action_system_id2', 
                                 'unit' => 'unit_test_action_unit2');
        $actions_array[] = array('action_code' => 'unit_test_action_code3',
                                 'action_name' => 'unit_test_action_name3', 
                                 'action_note' => 'unit_test_action_note3', 
                                 'system_id' => 'unit_test_action_system_id3', 
                                 'unit' => 'unit_test_action_unit3');
		$r = $LogisAAction->add_action_to_database($actions_array);
		$this->log($r, true, 'add_action_to_database');

        $sql_select = "select action_code, action_name, action_note, system_id, unit  
                    from  T_ACTION_INFO where action_code in('unit_test_action_code1',
                     'unit_test_action_code2', 'unit_test_action_code3')
                     order by action_code;";
        $select_result = Tools::get_query_result($sql_select);		                                         
		$this->log(count($select_result), 3, 'test_add_action');

		$r = $LogisAAction->remove_action_from_database($actions_array);
		$this->log($r, true, 'remove_action_from_database');

        $select_result = Tools::get_query_result($sql_select);		                                         
		$this->log(count($select_result), 0, 'test_remove_action');
	}
	public function log($b1, $b2, $text)
	{
		if($b1 == $b2){
			echo 'True   '.$text.'<br>';
		}
		else{
			echo 'Flase   '.$text.'<br>';
		}
	}
}

?>