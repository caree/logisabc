<?php

require_once('tools.php');

class CostDataHistoryAction extends Action
{
    public function company_action_cost_detail_index()
    {
        $history_id = Tools::request('history_id');
        $this->assign('history_id', $history_id);
        $this->display();
    }
    public function action_cost_detail_index()
    {
        $history_id = Tools::request('history_id');
        $this->assign('history_id', $history_id);
        $this->display();

    }
    public function list_action_history_cost_list()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        // $user_id = 'user1';
        $system_id = Tools::get_system_id();
        // $system_id = 'inventory';        
        echo $this->get_action_history_cost_json($history_id, $user_id, $system_id);        
    }
    public function export_excel_product_cost_history()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();

        // $history_id = 'product_report_20130308110813';
        // $user_id = 'user1';
        // $system_id = 'inventory';

        $json_str = $this->get_product_history_cost_json($history_id, $user_id, $system_id);
        $data = json_decode($json_str);
        $product_id = Tools::request("product_id");
        $sql_select = "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id in(
            select product_id from T_product_cost_history_link_product 
             where history_id = '$history_id' and user_id = '$user_id');";
        $list = Tools::get_query_result($sql_select);
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();        

        if(count($list) > 0){
            $row = $list[0];
            $objPHPExcel
                ->setActiveSheetIndex(0)
                    ->setCellValue('A3', '产品名称')
                    ->setCellValue('B3', $row['product_name'])
                    ->setCellValue('a4', '说明')
                    ->setCellValue('b4', $row['note'])
                    ->setCellValue('a5', '产量')
                    ->setCellValue('b5', $row['quantity']);                 
        }

        //首先把用户信息和ID输出
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '学生ID')
                                            ->setCellValue('B1', $user_id)
                                            ->setCellValue('c1', '历史记录ID')
                                            ->setCellValue('d1', $history_id);


        $objPHPExcel
            ->setActiveSheetIndex(0)->setCellValue('A7', '作业编码')
                                    ->setCellValue('B7', '作业名称')
                                    ->setCellValue('c7', '平均作业成本')
                                    ->setCellValue('d7', '作业使用量')
                                    ->setCellValue('e7', '单位')
                                    ->setCellValue('f7', '总作业成本');

        $total_length = count($data);
        $column_count = 6;
        $a_all_column_num = Tools::number_to_ABC_map('a', $column_count);

        for ($i= 0; $i < $total_length; $i++) { 
            $row = $data[$i];
            for($j = 0; $j < $column_count; $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 8;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->action_code);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->action_name);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->each_work_fee);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->action_count);
                        break;
                    case 'E':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->unit);
                        break;
                    case 'F':
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($column_index.$row_index, $row->action_cost);
                    break;
                }
            }            
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;            
    }
    public function export_excel_company_product_cost_history()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();
        // $user_id = 'user1';
        // $system_id = 'company';
        // $history_id = 'product_report_20130310082649';

        $array_result
             = json_decode($this->get_product_history_cost_json($history_id, $user_id, $system_id), true);

        $sql_select = "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id in(
            select product_id from T_product_cost_history_link_product 
             where history_id = '$history_id' and user_id = '$user_id');";            
        $list = Tools::get_query_result($sql_select);
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();        

        if(count($list) > 0){
            $row = $list[0];
            $objPHPExcel
                ->setActiveSheetIndex(0)
                    ->setCellValue('A3', '产品编码')
                    ->setCellValue('B3', $row['product_id'])
                    ->setCellValue('C3', '产品名称')
                    ->setCellValue('D3', $row['product_name'])
                    ->setCellValue('a4', '说明')
                    ->setCellValue('b4', $row['note'])
                    ->setCellValue('a5', '产量')
                    ->setCellValue('b5', $row['quantity']);                 
        }

        //首先把用户信息和ID输出
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '学生ID')
                                            ->setCellValue('B1', $user_id);


        $objPHPExcel
            ->setActiveSheetIndex(0)->setCellValue('A7', '系统名称')
                                    ->setCellValue('B7', '总作业成本')
                                    ->setCellValue('c7', '作业使用量')
                                    ->setCellValue('d7', '平均作业成本');

        $total_length = count($array_result);
        $column_count = 6;
        $a_all_column_num = Tools::number_to_ABC_map('a', $column_count);
        $i = -1;
        foreach ($array_result as $key => $row) {
            $i = $i + 1;
            for($j = 0; $j < $column_count; $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 8;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['system_name']);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['total_action_cost']);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['action_count']);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['each_work_fee']);
                        break;
                }
            }                  
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;           
    }
    public function export_excel_company_action_cost_history()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();
        // $history_id = 'report_20130308120340';
        // $user_id = 'user1';
        // $system_id = 'inventory';
        $array_action_cost_list = json_decode($this->get_action_history_cost_json($history_id, $user_id, $system_id));
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();
        //设置列标题
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '系统名称')
                                            ->setCellValue('B1','总作业成本')
                                            ->setCellValue('c1', '总处理量')
                                            ->setCellValue('d1', '平均作业成本');


        $a_all_column_num = Tools::number_to_ABC_map('a', count($array_action_cost_list));

        //输入数据
        $i = -1;
        foreach ($array_action_cost_list as $key => $value) {
            $i ++;
            for($j = 0; $j < count($a_all_column_num); $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 2;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value->system_name);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value->total_action_cost);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value->total_action_work_capability);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $value->each_action_cost);
                        break;
                }
            }
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;              
    }
    public function export_excel_action_cost_history()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();
        // $history_id = 'report_20130308120340';
        // $user_id = 'user1';
        // $system_id = 'inventory';
        $data = json_decode($this->get_action_history_cost_json($history_id, $user_id, $system_id));

        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();        
        //首先把用户信息和ID输出
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '学生ID')
                                            ->setCellValue('B1', $user_id)
                                            ->setCellValue('c1', '历史记录ID')
                                            ->setCellValue('d1', $history_id);

        //输出具体数据
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', '作业编码')
                                            ->setCellValue('B3', '作业名称')
                                            ->setCellValue('c3', '总作业成本')
                                            ->setCellValue('d3', '月处理量')
                                            ->setCellValue('e3', '单位')
                                            ->setCellValue('f3', '单位处理成本');

        // var_dump($data);
        $total_length = count($data);
        $column_count = 6;
        $a_all_column_num = Tools::number_to_ABC_map('a', $column_count);

        for ($i= 0; $i < $total_length; $i++) { 
            $row = $data[$i];
            for($j = 0; $j < $column_count; $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 4;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->action_code);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->action_name);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->action_total_fee);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->action_work_capability);
                        break;
                    case 'E':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row->unit);
                        break;
                    case 'F':
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($column_index.$row_index, $row->each_work_fee);
                    break;
                }
            }            
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;                
    }
    public function company_product_cost_detail_index()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $product_id = Tools::request("product_id");
        $sql_select = "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id in(
            select product_id from T_product_cost_history_link_product 
             where history_id = '$history_id' and user_id = '$user_id');";
        $list = Tools::get_query_result($sql_select);
        if(count($list) > 0){
            $row = $list[0];
            $this->assign('product_name', $row['product_name']);
            $this->assign('quantity', $row['quantity']);
            if(empty($row['note'])){
                $this->assign('note', '尚未添加说明');
            }
            else{
                $this->assign('note', $row['note']);
            }
            $this->assign('product_id', $row['product_id']);        
        }
        $this->assign('history_id', $history_id);
        $this->display();        
    }
    public function list_product_cost_detail_index()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $product_id = Tools::request("product_id");
        $sql_select = "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id in(
            select product_id from T_product_cost_history_link_product 
             where history_id = '$history_id' and user_id = '$user_id');";
        $list = Tools::get_query_result($sql_select);
        if(count($list) > 0){
            $row = $list[0];
            $this->assign('product_name', $row['product_name']);
            $this->assign('quantity', $row['quantity']);
            if(empty($row['note'])){
                $this->assign('note', '尚未添加说明');
            }
            else{
                $this->assign('note', $row['note']);
            }
            $this->assign('product_id', $row['product_id']);        
        }
        $this->assign('history_id', $history_id);
        $this->display();
    }
    public function list_product_history_cost_data()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        // $user_id = 'user1';
        $system_id = Tools::get_system_id();
        // $system_id = 'inventory';
        $sql = "select history from T_product_cost_history 
             where history_id='$history_id' and user_id = '$user_id' 
             and system_id = '$system_id';";
        // echo $sql;
        $list = Tools::get_query_result($sql);
        if(count($list) >0 )
        {
            echo $list[0]['history'];
        }
    }

    public function get_chart_data_product_history_cost_data()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();

        $data = json_decode($this->get_product_history_cost_json($history_id, $user_id, $system_id), true);
        $pies_data_array = array();
        foreach ($data as $key => $item) {
            $pies_data_array[] = "['".$item['action_name']."',".Tools::format_number_to_fload($item['action_cost'])."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['action_name']."',".Tools::format_number_to_fload($item['action_cost'])."]";
        // }, $data);

        echo '['.implode(',',$pies_data_array).']'; return;


        // $colors = Tools::randomColors(count($data));

        // $total_fee = 0;
        // array_walk($data, function($item, $key) use(&$total_fee){
        //     $total_fee += Tools::format_number_to_fload($item->action_cost);
        // });

        // $pies = array_map(function($item) use($total_fee){
        //     $action_cost = Tools::format_number_to_fload($item->action_cost);
        //     if($action_cost > 0){
        //         $percent = number_format(100 * $action_cost / $total_fee, 2);
        //     }
        //     return array('label' => $item->action_name.'('.$percent.'%)', 
        //                     'value' => $action_cost);                
        // }, $data);
        // $pies_json = json_encode($pies);
        // echo '{
        //       "elements":[
        //         {
        //           "type":      "pie",
        //           "colours":   '
        //           .json_encode($colors).',
        //           "alpha":     0.6,
        //           "border":    2,
        //           "start-angle": 35,
        //           "values" :   '
        //           .$pies_json.'
        //         }
        //       ]
        //     }';
    }
    public function get_chart_data_company_product_history_cost_data()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();
        // $user_id = 'user1';
        // $system_id = 'company';
        // $history_id = 'product_report_20130310082649';

        $system_product_cost_array
             = json_decode($this->get_product_history_cost_json($history_id, $user_id, $system_id), true);
        $pies_data_array = array();
        foreach ($system_product_cost_array as $key => $item) {
            $pies_data_array[] = "['".$item['system_name']."',".Tools::format_number_to_fload($item['total_action_cost'])."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['system_name']."',".Tools::format_number_to_fload($item['total_action_cost'])."]";
        // }, $system_product_cost_array);

        echo '['.implode(',',$pies_data_array).']'; return;        

        // $all_action_total_fee = 0;

        // array_walk($system_product_cost_array, function($value, $i) use(&$all_action_total_fee){
        //     $action_cost = $value['total_action_cost'];
        //     $all_action_total_fee += $action_cost;
        // });

        // $pies = array_map(function($item) use($all_action_total_fee){
        //     $action_cost = $item['total_action_cost'];
        //     $percent = 0;
        //     if($all_action_total_fee > 0)
        //     {
        //         $percent = number_format(100 * $action_cost / $all_action_total_fee, 2);
        //     }
        //     return array('label' => $item['system_name'].'('.$percent.'%)', 
        //                     'value' => Tools::format_number_to_fload($item['total_action_cost']));
        // }, $system_product_cost_array);
        // $pies_json = json_encode(array_values($pies));
        // $colors = Tools::randomColors(count($system_product_cost_array));
        
        // echo '{
        //   "elements":[
        //     {
        //       "type":      "pie",
        //       "colours":   '
        //       .json_encode($colors).',
        //       "alpha":     0.6,
        //       "border":    2,
        //       "start-angle": 35,
        //       "values" :   '
        //       .$pies_json.'
        //     }
        //   ]
        // }';        
    }
    public function get_chart_data_company_action_history_cost()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();

        $data = json_decode($this->get_action_history_cost_json($history_id, $user_id, $system_id), true);
        $pies_data_array = array();
        foreach ($data as $key => $item) {
            $pies_data_array[] = "['".$item['system_name']."',".Tools::format_number_to_fload($item['total_action_cost'])."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['system_name']."',".Tools::format_number_to_fload($item['total_action_cost'])."]";
        // }, $data);
        echo '['.implode(',',$pies_data_array).']'; return;        

        // $colors = Tools::randomColors(count($data));

        // $total_fee = 0;
        // array_walk($data, function($item, $key) use(&$total_fee){
        //     $total_fee += Tools::format_number_to_fload($item->total_action_cost);
        // });

        // $pies = array_map(function($item) use($total_fee){
        //     $action_total_fee = Tools::format_number_to_fload($item->total_action_cost);
        //     if($total_fee > 0){
        //         $percent = number_format(100 * $action_total_fee / $total_fee, 2);
        //     }
        //     return array('label' => $item->system_name.'('.$percent.'%)', 
        //                     'value' => Tools::format_number_to_fload($action_total_fee));                
        // }, $data);
        // $pies_json = json_encode($pies);
        // echo '{
        //       "elements":[
        //         {
        //           "type":      "pie",
        //           "colours":   '
        //           .json_encode($colors).',
        //           "alpha":     0.6,
        //           "border":    2,
        //           "start-angle": 35,
        //           "values" :   '
        //           .$pies_json.'
        //         }
        //       ]
        //     }';        
    }
    public function get_chart_data_action_history_cost()
    {
        $history_id = Tools::request('history_id');
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();

        $data = json_decode($this->get_action_history_cost_json($history_id, $user_id, $system_id), true);
        // var_dump($data[0]->action_name);return;
        $pies_data_array = array();
        foreach ($data as $key => $item) {
            $pies_data_array[] = "['".$item['action_name']."',".Tools::format_number_to_fload($item['action_total_fee'])."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['action_name']."',".Tools::format_number_to_fload($item['action_total_fee'])."]";
        // }, $data);

        echo '['.implode(',',$pies_data_array).']'; return;

        // $colors = Tools::randomColors(count($data));

        // $total_fee = 0;
        // array_walk($data, function($item, $key) use(&$total_fee){
        //     $total_fee += Tools::format_number_to_fload($item->action_total_fee);
        // });
        // $pies = array();
        // foreach ($data as $key => $item) {
        //     $action_total_fee = Tools::format_number_to_fload($item->action_total_fee);
        //     if($action_total_fee > 0){
        //         $percent = number_format(100 * $action_total_fee / $total_fee, 2);
        //     }
        //     $pies[] = array('label' => $item->action_name.'('.$percent.'%)', 
        //                     'value' => $action_total_fee);                
        // }
        // $pies = array_map(function($item) use($total_fee){
        //     $action_total_fee = Tools::format_number_to_fload($item->action_total_fee);
        //     if($action_total_fee > 0){
        //         $percent = number_format(100 * $action_total_fee / $total_fee, 2);
        //     }
        //     return array('label' => $item->action_name.'('.$percent.'%)', 
        //                     'value' => $action_total_fee);                
        // }, $data);
        // $pies_json = json_encode($pies);
        // echo '{
        //       "elements":[
        //         {
        //           "type":      "pie",
        //           "colours":   '
        //           .json_encode($colors).',
        //           "alpha":     0.6,
        //           "border":    2,
        //           "start-angle": 35,
        //           "values" :   '
        //           .$pies_json.'
        //         }
        //       ]
        //     }';
    }
    public function get_chart_data()
    {
        $user_id = Tools::get_user_data_set();
        $history_id = Tools::request('history_id');
        if(!empty($history_id)){
            $sql_select 
                = "select T1.history_id, T1.action_code, T1.action_fee, T2.action_name 
                 from T_ACTION_LINK_HISTORY T1, T_ACTION_INFO T2 
                  where T1.history_id = '$history_id' and T1.action_code = T2.action_code and T1.user_id = '$user_id';";
            $array_result = Tools::get_query_result($sql_select);
            $total_fee = 0;
            array_walk($array_result, function($item, $key) use(&$total_fee){
                $total_fee += $item['action_fee'];
            });
            $pies = array_map(function($item) use($total_fee){
                $action_total_fee = $item['action_fee'];
                if($action_total_fee > 0){
                    $percent = number_format(100 * $action_total_fee / $total_fee, 2);
                }
                return array('label' => $item['action_name'].'('.$percent.'%)', 
                                'value' => $item['action_fee'] + 0);                
            }, $array_result);
            $pies_json = json_encode($pies);

            $colors = Tools::randomColors(count($array_result));
            echo '{
              "elements":[
                {
                  "type":      "pie",
                  "colours":   '
                  .json_encode($colors).',
                  "alpha":     0.6,
                  "border":    2,
                  "start-angle": 35,
                  "values" :   '
                  .$pies_json.'
                }
              ]
            }';
        }
    }
    public function remove_product_history()
    {
        $json = Tools::request("data");
        $user_id = Tools::get_user_data_set();
        // echo $json;return;
            // $history_id = $json[0]['history_id'];
        $flag = $this->remove_product_history_from_database($json, $user_id);
        if (!$flag) {
            $foo_json = Tools::set_result_json('failed', '删除数据时出现异常');
        }
        else{
            $foo_json = Tools::set_result_json('ok', '删除数据成功！');
        }
        echo $foo_json;         
    }
    public function remove_history()
    {
        $json_str = Tools::request("data");
        // $json = Tools::request_json("data");
        $system_id = Tools::get_system_id();
        $user_id = Tools::get_user_data_set();
        // echo $json;return;
        $json = json_decode($json_str, true);
        if(Tools::json_is_array($json_str)){
            if (count($json) > 0) {
                $history_id = $json[0]['history_id'];
                $flag = $this->remove_history_from_database($history_id, $user_id, $system_id);
            }
        }
        else{
            $history_id = $json['history_id'];
            $flag = $this->remove_history_from_database($history_id, $user_id, $system_id);
        }
            // echo $flag;return;
        if (!$flag) {
            $foo_json = Tools::set_result_json('failed', '删除数据时出现异常');
        }
        else{
            $foo_json = Tools::set_result_json('ok', '删除数据成功！');
        }
        echo $foo_json;          
    }
    public function cost_data_history_list()
    {
        $user_id = Tools::get_user_data_set();
        // $user_id = 'user1';
        // $result = array();
        $system_id = Tools::get_system_id();
        $sql_select_cost_data_history = 
            "select history_id, time_stamp from T_ACTION_COST_HISTORY
             where user_id = '$user_id' and system_id = '$system_id';";
        $list_data_history = Tools::get_query_result($sql_select_cost_data_history);
        $result = array();
        foreach ($list_data_history as $key => $item) {
                $history_id = $item['history_id'];
                $sql_select = "select count(action_code) count from T_ACTION_LINK_HISTORY where history_id = '$history_id' and user_id = '$user_id';";
                $list = Tools::get_query_result($sql_select);
                $count = $list[0]['count'];
                $result[] = array('history_id'=>$history_id
                            , 'time_stamp'=>$item['time_stamp']
                            , 'count'=>$count);
        }
        // $result = array_map(function($item) use($user_id){
        //         $history_id = $item['history_id'];
        //         $sql_select = "select count(action_code) count from T_ACTION_LINK_HISTORY where history_id = '$history_id' and user_id = '$user_id';";
        //         $list = Tools::get_query_result($sql_select);
        //         $count = $list[0]['count'];
        //         return array('history_id'=>$history_id
        //                     , 'time_stamp'=>$item['time_stamp']
        //                     , 'count'=>$count);

        // }, $list_data_history);

        $foo_json = json_encode($result);
        echo $foo_json;  
    }

    public function company_action_cost_data_history_index()
    {
        $this->display();
    }
    public function cost_data_history_index()
    {
        $this->display();
    }
    public function company_product_cost_data_history_index()
    {
        $this->display();
    }
    public function product_cost_data_history_index()
    {
        $this->display();
    }
    public function product_cost_data_history_list()
    {
        $user_id = Tools::get_user_data_set();
        $system_id = Tools::get_system_id();
        // $user_id = 'user1';
        // $result = array();
        $sql_select_cost_data_history = 
            "select history_id, time_stamp from T_product_cost_history 
             where user_id = '$user_id' and system_id = '$system_id';";
        $list_data_history = Tools::get_query_result($sql_select_cost_data_history);

        $foo_json = json_encode($list_data_history);
        echo $foo_json;          
    }
    public function save_product_history_data()
    {
        $product_id = Tools::request('product_id');
        $json_str = Tools::request("data");
        if(empty($json_str)){
            $foo_json = Tools::set_result_json('failed', '没有数据被保存！');
        }
        else{
            $user_id = Tools::get_user_data_set();
            $system_id = Tools::get_system_id();
            $flag = 
                $this->save_product_cost_history($json_str, $user_id, $system_id, $product_id);
            // echo $flag; return;
            if($flag){
                $foo_json = Tools::set_result_json('ok', '数据已保存, 请在产品成本历史页面查看。');
            }
            else{
                $foo_json = Tools::set_result_json('failed', '插入数据时出现异常');
            }
        }
        echo $foo_json;  
    }    
    /**
    +----------------------------------------------------------
    * 数据计算输出
    +----------------------------------------------------------
    */

    function get_product_history_cost_json($history_id, $user_id, $system_id)
    {
        $sql = "select history from T_product_cost_history 
             where history_id='$history_id' and user_id = '$user_id' 
             and system_id = '$system_id';";
        // echo $sql;
         // return $sql;
        $list = Tools::get_query_result($sql);
        if(count($list) >0 )
        {
            return $list[0]['history'];
        }            
    }    
    function get_action_history_cost_json($history_id, $user_id, $system_id)
    {

        $sql = "select history from T_ACTION_COST_HISTORY 
             where history_id='$history_id' and user_id = '$user_id' 
             and system_id = '$system_id';";
        // echo $sql;
        $list = Tools::get_query_result($sql);
        if(count($list) >0 )
        {
            return $list[0]['history'];
        }        
    }    
    public function save_product_cost_history($product_costs_str, $user_id, $system_id, $product_id)
    {
        //以日期作为键值，检查现有保存数据，则替换已有的
        date_default_timezone_set("Asia/Shanghai");
        $time = date("Y-m-d H:i:s");
        $data_id= 'product_report_'.date("YmdHis");
        $sql_replace = "replace into T_product_cost_history(history_id,user_id,system_id,time_stamp,history) 
            values('$data_id', '$user_id','$system_id', '$time','$product_costs_str');";
        $sql_replace .= "replace into T_product_cost_history_link_product(history_id,product_id,user_id) 
            values('$data_id', '$product_id', '$user_id');";
        // return $sql_replace;
        return Tools::trans_sql($sql_replace);
    }    
    function remove_product_history_from_database($history_id_json, $user_id)
    {
        $ids = "''";
        if(Tools::json_is_array($history_id_json)){
            $history_id_array = json_decode($history_id_json, true);
            foreach ($history_id_array as $key => $history) {
                $ids .= ",'".$history['history_id']."'";
            }
            // array_walk($history_id_array, function($history, $key) use(&$ids){
            //     $ids .= ",'".$history['history_id']."'";
            // });        
            $sql_delete_history = "delete from T_product_cost_history where history_id in($ids);";
            $sql_delete_history .= "delete from T_product_cost_history_link_product where history_id in($ids) 
                and user_id = '$user_id';";
        }
        else{
            $history = json_decode($history_id_json, true);
            $ids .= ",'".$history['history_id']."'";
            $sql_delete_history = "delete from T_product_cost_history where history_id in($ids);";
            $sql_delete_history .= "delete from T_product_cost_history_link_product where history_id in($ids) 
                and user_id = '$user_id';";
        }
        // return $sql_delete_history;
        return  Tools::trans_sql($sql_delete_history);
    }
    function remove_history_from_database($history_id, $user_id, $system_id)
    {
        $sql_delete_history 
            = "delete from T_ACTION_COST_HISTORY 
            where history_id = '$history_id' and user_id = '$user_id' and system_id = '$system_id';";
        // $sql_delete_history .= "delete from T_ACTION_LINK_HISTORY where history_id = '$history_id';";
        return  (Tools::trans_sql($sql_delete_history) > 0) ? true:false;
    }


}
?>