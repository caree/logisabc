<?php

class TreeNode
{
    public $id;//编码
    public $text;//名称
    public $expanded;//
    public $url = '';
    public $target = '_blank';
    public $pid;//
    public $iconCls = '';
    public $iconPosition = '';
    function __construct($id = '', $text = '', $url = '', $pid = '', $expanded = true)
    {
        $this->id = $id;
        $this->text = $text;
        $this->url = $url;
        $this->pid = $pid;
        $this->expanded = $expanded;
    }
}


?>