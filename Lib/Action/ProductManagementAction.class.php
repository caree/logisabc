<?php
require_once('CostCaculateAction.class.php');
require_once('tools.php');
class ProductManagementAction extends Action
{
    public function list_product_final_cost_index_viewer()
    {
        $user_id = Tools::request('user_id');
        $product_id = Tools::request('product_id');
        $this->assign('user_id', $user_id);
        $this->assign('product_id', $product_id);
        $sql_select = 
            "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id = '$product_id';";
        $list = Tools::get_query_result($sql_select);
        if(count($list) > 0){
            $row = $list[0];
            $this->assign('product_name', $row['product_name']);
            $this->assign('quantity', $row['quantity']);
            if(empty($row['note'])){
                $this->assign('note', '尚未添加说明');
            }
            else{
                $this->assign('note', $row['note']);
            }
        }
        $this->display();
    }
    public function product_final_cost_company_index_user()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::get_user_data_set();
        $this->assign('user_id', $user_id);
        $this->assign('product_id', $product_id);
        $sql_select = 
            "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id = '$product_id';";
        $list = Tools::get_query_result($sql_select);
        if(count($list) > 0){
            $row = $list[0];
            $this->assign('product_name', $row['product_name']);
            $this->assign('quantity', $row['quantity']);
            if(empty($row['note'])){
                $this->assign('note', '尚未添加说明');
            }
            else{
                $this->assign('note', $row['note']);
            }
        }        
        $this->display();        
    }
    public function product_final_cost_company_index_viewer()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::request('user_id');
        $this->assign('user_id', $user_id);
        $this->assign('product_id', $product_id);
        $sql_select = 
            "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id = '$product_id';";
        $list = Tools::get_query_result($sql_select);
        if(count($list) > 0){
            $row = $list[0];
            $this->assign('product_name', $row['product_name']);
            $this->assign('quantity', $row['quantity']);
            if(empty($row['note'])){
                $this->assign('note', '尚未添加说明');
            }
            else{
                $this->assign('note', $row['note']);
            }
        }        
        $this->display();
    }
    public function list_product_final_cost_company_viewer()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::request('user_id');
        // $product_id = '1001';
        // $user_id = 'user1';
        $system_product_cost_array = $this->get_product_company_cost($product_id, $user_id);
        // var_dump($system_product_cost_array);
        echo json_encode($system_product_cost_array);
    }
    public function get_chart_data_company_product_cost_viewer()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::request('user_id');
        // $product_id = '1001';
        // $user_id = 'user1';
        $system_product_cost_array = $this->get_product_company_cost($product_id, $user_id);

        $pies_data_array = array();
        foreach ($system_product_cost_array as $key => $item) {
            $pies_data_array[] =  "['".$item['system_name']."',".$item['total_action_cost']."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['system_name']."',".$item['total_action_cost']."]";
        // }, $system_product_cost_array);

        echo '['.implode(',',$pies_data_array).']'; return;

        // $all_action_total_fee = 0;

        // array_walk($system_product_cost_array, function($value, $i) use(&$all_action_total_fee){
        //     $action_cost = $value['total_action_cost'];
        //     $all_action_total_fee += $action_cost;
        // });
        // $pies = array();
        // foreach ($system_product_cost_array as $key => $item) {
        //     $action_cost = $item['total_action_cost'];
        //     $percent = 0;
        //     if($all_action_total_fee > 0)
        //     {
        //         $percent = number_format(100 * $action_cost / $all_action_total_fee, 2);
        //     }
        //     $pies[] = array('label' => $item['system_name'].'('.$percent.'%)', 
        //                     'value' => Tools::format_number_to_fload($item['total_action_cost']));

        // }
        // $pies = array_map(function($item) use($all_action_total_fee){
        //     $action_cost = $item['total_action_cost'];
        //     $percent = 0;
        //     if($all_action_total_fee > 0)
        //     {
        //         $percent = number_format(100 * $action_cost / $all_action_total_fee, 2);
        //     }
        //     return array('label' => $item['system_name'].'('.$percent.'%)', 
        //                     'value' => Tools::format_number_to_fload($item['total_action_cost']));
        // }, $system_product_cost_array);
        // var_dump($pies); return;
        // $pies_json = json_encode(array_values($pies));
        // $colors = Tools::randomColors(count($system_product_cost_array));
        
        // echo '{
        //   "elements":[
        //     {
        //       "type":      "pie",
        //       "colours":   '
        //       .json_encode($colors).',
        //       "alpha":     0.6,
        //       "border":    2,
        //       "start-angle": 35,
        //       "values" :   '
        //       .$pies_json.'
        //     }
        //   ]
        // }';
    }
    function get_product_company_cost($product_id, $user_id)
    {
        //把各个系统耗费的成本汇总到一起
       $system_product_cost_array = array();
        $system_list_array = Tools::system_list_array();
        $array_product_cost_list = array();
        foreach ($system_list_array as $key => $system) {
            $array_system_product_cost_list 
                = CostCaculateAction::get_product_final_cost($product_id, $system['id'], $user_id);
            $action_cost = 0;
            $action_count = 0;
            foreach ($array_system_product_cost_list as $k => $value) {
                $action_cost += Tools::format_number_to_fload($value['action_cost']);
                $action_count += Tools::format_number_to_fload($value['action_count']);
            }
            $each_work_fee = 0;
            if($action_count > 0){
                $each_work_fee = $action_cost/$action_count;
            }
            $system_product_cost_array[] = array(
                'system_name' => $system['text'],
                'total_action_cost' => number_format($action_cost, 2),
                'action_count' => number_format($action_count, 2),
                'each_work_fee' => number_format($each_work_fee, 2));
        }
        return $system_product_cost_array;
    }
    public function list_product_final_cost_viewer()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::request('user_id');
        $system_id = Tools::get_system_id();


        $array_product_cost_list = 
            CostCaculateAction::get_product_final_cost($product_id, $system_id, $user_id);
        $result = array();
        foreach ($array_product_cost_list as $k => $product_cost) {
            foreach ($product_cost as $key => $value) {
                if(strpos($key, '_fee')){
                    $product_cost[$key] = number_format($value, 2);
                }
            }
            $product_cost['action_cost'] = number_format($product_cost['action_cost'], 2);
            $result[] = $product_cost;
        }
        // $result = array_map(function($product_cost){
        //     foreach ($product_cost as $key => $value) {
        //         if(strpos($key, '_fee')){
        //             $product_cost[$key] = number_format($value, 2);
        //         }
        //     }
        //     $product_cost['action_cost'] = number_format($product_cost['action_cost'], 2);
        //     return $product_cost;
        // }, $array_product_cost_list);

        // $system_id = Tools::get_system_id();
        // $array_result = CostCaculateAction::get_product_final_cost($product_id, $system_id, $user_id);
        // array_walk($array_result, function($value, $key) use(&$array_result){
        //     $array_result[$key]['each_work_fee'] = number_format($value['each_work_fee'], 2);
        //     $array_result[$key]['action_cost'] = number_format($value['action_cost'], 2);
        // });
        // for ($i=0; $i < count($array_result); $i++) { 
        //     $array_result[$i]['each_work_fee'] = number_format($array_result[$i]['each_work_fee'], 2);
        //     $array_result[$i]['action_cost'] = number_format($array_result[$i]['action_cost'], 2);
        // }
        $foo_json = json_encode(array_values($result));
        echo $foo_json;       

    }
    public function export_excel_company_product_cost_viewer()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::request('user_id');
        // $product_id = '1001';
        // $user_id = 'user1';
        $array_result = $this->get_product_company_cost($product_id, $user_id);
        $sql_select = "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id = '$product_id';";
        $list = Tools::get_query_result($sql_select);
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();        

        if(count($list) > 0){
            $row = $list[0];
            $objPHPExcel
                ->setActiveSheetIndex(0)
                    ->setCellValue('A3', '产品编码')
                    ->setCellValue('B3', $product_id)
                    ->setCellValue('C3', '产品名称')
                    ->setCellValue('D3', $row['product_name'])
                    ->setCellValue('a4', '说明')
                    ->setCellValue('b4', $row['note'])
                    ->setCellValue('a5', '产量')
                    ->setCellValue('b5', $row['quantity']);                 
        }

        //首先把用户信息和ID输出
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '学生ID')
                                            ->setCellValue('B1', $user_id);


        $objPHPExcel
            ->setActiveSheetIndex(0)->setCellValue('A7', '系统名称')
                                    ->setCellValue('B7', '总作业成本')
                                    ->setCellValue('c7', '作业使用量')
                                    ->setCellValue('d7', '平均作业成本');

        $total_length = count($array_result);
        $column_count = 6;
        $a_all_column_num = Tools::number_to_ABC_map('a', $column_count);
        $i = -1;
        foreach ($array_result as $key => $row) {
            $i = $i + 1;
            for($j = 0; $j < $column_count; $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 8;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['system_name']);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['total_action_cost']);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['action_count']);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['each_work_fee']);
                        break;
                }
            }                  
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;             
    }
    public function export_excel_product_cost_viewer()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::request('user_id');
        $system_id = Tools::get_system_id();        
        $this->export_excel_product_cost($product_id, $system_id, $user_id);
    }
    function export_excel_product_cost($product_id, $system_id, $user_id)
    {
        $array_result = CostCaculateAction::get_product_final_cost($product_id, $system_id, $user_id);
        foreach ($array_result as $key => $value) {
            $array_result[$key]['each_work_fee'] = number_format($value['each_work_fee'], 2);
            $array_result[$key]['action_cost'] = number_format($value['action_cost'], 2);
        }
        // var_dump($array_result);return;
        $sql_select = "select T1.product_id, T1.product_name, T1.note, T1.quantity 
            from T_PRODUCT T1 where T1.product_id = '$product_id';";
        $list = Tools::get_query_result($sql_select);
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();        

        if(count($list) > 0){
            $row = $list[0];
            $objPHPExcel
                ->setActiveSheetIndex(0)
                    ->setCellValue('A3', '产品编码')
                    ->setCellValue('B3', $product_id)
                    ->setCellValue('C3', '产品名称')
                    ->setCellValue('D3', $row['product_name'])
                    ->setCellValue('a4', '说明')
                    ->setCellValue('b4', $row['note'])
                    ->setCellValue('a5', '产量')
                    ->setCellValue('b5', $row['quantity']);                 
        }

        //首先把用户信息和ID输出
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '学生ID')
                                            ->setCellValue('B1', $user_id);


        $objPHPExcel
            ->setActiveSheetIndex(0)->setCellValue('A7', '作业编码')
                                    ->setCellValue('B7', '作业名称')
                                    ->setCellValue('c7', '平均作业成本')
                                    ->setCellValue('d7', '作业使用量')
                                    ->setCellValue('e7', '单位')
                                    ->setCellValue('f7', '总作业成本');

        $total_length = count($array_result);
        $column_count = 6;
        $a_all_column_num = Tools::number_to_ABC_map('a', $column_count);
        $i = -1;
        foreach ($array_result as $key => $row) {
            $i = $i + 1;
            for($j = 0; $j < $column_count; $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 8;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['action_code']);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['action_name']);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['each_work_fee']);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['action_count']);
                        break;
                    case 'E':
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($column_index.$row_index, $row['unit']);
                        break;
                    case 'F':
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($column_index.$row_index, $row['action_cost']);
                    break;
                }
            }                  
        }
        // for ($i= 0; $i < $total_length; $i++) { 
        //     $row = $array_result[$i];
      
        // }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;       
    }
    public function export_excel_product_cost_user()
    {
        $product_id = Tools::request('product_id');
        $system_id = Tools::get_system_id();
        $user_id = Tools::get_user_data_set();

        // $system_id = 'inventory';
        // $user_id = 'user1';
        $this->export_excel_product_cost($product_id, $system_id, $user_id);
 
    }
    public function list_product_final_cost()
    {
        $product_id = Tools::request('product_id');
        $system_id = Tools::get_system_id();
        $user_id = Tools::get_user_data_set();

        $array_result = CostCaculateAction::get_product_final_cost($product_id, $system_id, $user_id);
        foreach ($array_result as $key => $value) {
            $array_result[$key]['each_work_fee'] = number_format($value['each_work_fee'], 2);
            $array_result[$key]['action_cost'] = number_format($value['action_cost'], 2);
        }
        $foo_json = json_encode(array_values($array_result));

        echo $foo_json;       
    }

    //教师获取产品成本图表统计数据
    public function get_chart_data_product_cost_viewer()
    {
        $product_id = Tools::request('product_id');
        $user_id = Tools::request('user_id');
        $system_id = Tools::get_system_id();
        echo $this->get_chart_data_product_cost($product_id, $system_id, $user_id);
    }
    //学生获取产品成本图表统计数据
    public function user_get_chart_data_product_cost()
    {
        $product_id = Tools::request('product_id');
        $system_id = Tools::get_system_id();
        $user_id = Tools::get_user_data_set();
        // $user_id = 'user1';
        // $system_id = 'inventory';
        // $product_id = '1001';
        // var_dump(Tools::get_system_info_by_id(Tools::system_list_array(), $system_id));return;
        echo $this->get_chart_data_product_cost($product_id, $system_id, $user_id);
    }
   
    //产品成本统计
    public function list_product_final_cost_index()
    {
        $product_id = Tools::request("product_id");
        $sql_select = "select T1.product_id, T1.product_name, T1.note, T1.quantity from T_PRODUCT T1 where T1.product_id = '$product_id';";
        $M = new Model();
        $list = $M->query($sql_select);
        if(count($list) > 0)
        {
            $row = $list[0];
            $this->assign('product_name', $row['product_name']);
            $this->assign('quantity', $row['quantity']);
            if(empty($row['note']))
            {
                $this->assign('note', '尚未添加说明');
            }
            else
            {
                $this->assign('note', $row['note']);
            }
        }
        $this->assign('product_id', $product_id);
        $this->display();
    }

    public function ProductIndex_admin()
    {
        $this->display();
    }
    public function product_index_for_company_cost_result()
    {
        $this->display();
    }
    //产品列表，可以通过该页打开产品成本统计结果
    public function product_index_for_cost_result()
    {
        $this->display();
    }

    public function remove_product()
    {
        $json = Tools::request("data");
        $flag = $this->remove_product_from_database($json);
        echo $flag ? 'ok':'failed';
    }

    public function add_product()
    {
        $json = Tools::request("data");
        $flag = $this->add_product_to_database($json);
        echo $flag ? 'ok':'failed';
    }
    //更新每个产品的作业量
    public function save_product_action_count_setting()
    {
        $json = Tools::request("data");
        // $flag = true;
        $user_id = $_SESSION['acount'];
        $flag = $this->save_product_action_count_setting_to_database($json, $user_id);
        echo $flag ? 'ok':'failed';
    }

    //显示当前用户在产品中对具体作业作业量的分配，返回所有产品当前分配量的列表
    public function list_product_action_count()
    {
        $action_code = Tools::request('action_code');
        if(!empty($action_code)){
            // $system_id = $_SESSION['system_id'];
            $system_id = Tools::get_system_id();
            // $user_data_set = $_SESSION['user_data_set'];
            $user_data_set = Tools::get_user_data_set();
            $sql_select = "select T1.product_id, T1.product_name, T1.note, ifnull(T2.action_count, 0) action_count 
                            from T_PRODUCT T1 left join T_PRODUCT_LINK_INFO T2  on T1.product_id = T2.product_id 
                             and T2.action_code = '$action_code' and T2.user_id = '$user_data_set';";
            $select_result = Tools::get_query_result($sql_select);

            $foo_json = json_encode($select_result);            
        }

        echo $foo_json;        
    }
    //
    // 学生查看具体产品的成本时提供产品列表
    public function product_list_user()
    {
        $user_data_set = Tools::get_user_data_set();
        $sql_select = "select product_id, product_name, note, quantity from T_PRODUCT";

        $select_result = Tools::get_query_result($sql_select);
        $foo_json = json_encode($select_result);
        echo $foo_json;        
    }
    public function product_list()
    {
        $sql_select = "select product_id, product_name, note, quantity from T_PRODUCT";
        $select_result = Tools::get_query_result($sql_select);
        $foo_json = json_encode($select_result);
        echo $foo_json;        
    }

    //显示当前用户在产品中对具体作业作业量的分配，
    public function product_action_count_setting_index()
    {
        // $action_code = Tools::request('action_code');
        // $system_id = $_SESSION['system_id'];
        // $user_data_set = $_SESSION['user_data_set'];
        // $user_data_set = Tools::get_system_id();
        // $sql_select = "select T1.action_code, T1.action_name, T1.action_note, ifnull(T2.action_work_capability, 0) action_work_capability 
        //             from  T_ACTION_INFO T1 left join T_ACTION_LINK_WORK_CAPABILITY T2 on T1.action_code = T2.action_code and T2.user_id = '$user_data_set'
        //              where T1.action_code = '$action_code';";

        // $select_result = Tools::get_query_result($sql_select);
        // if (count($select_result) > 0) {
        //     $row = $select_result[0];
        //     $action_name = $row['action_name'];
        //     $action_work_capability = $row['action_work_capability'];
        //     $action_note = $row['action_note'];

        //     $this->assign('action_name', $action_name);
        //     $this->assign('action_note', $action_note);
        //     $this->assign('action_work_capability', $action_work_capability);
        // }
        // $this->assign('action_code', $action_code);
        $this->display();
    }


    /**
    +----------------------------------------------------------
    * 数据计算输出
    +----------------------------------------------------------
    */

    function save_product_action_count_setting_to_database($products_json, $user_id)
    {
        $sql_update = "";
        if(Tools::json_is_array($products_json)){
            $products_array = json_decode($products_json, true);
            foreach ($products_array as $key => $product) {
                $product_id = $product['product_id'];
                $action_code = $product['action_code'];
                $action_count = $product['action_count'];
                $sql_update .= "replace into T_PRODUCT_LINK_INFO(product_id, action_code, user_id, action_count) values('$product_id', '$action_code', '$user_id', $action_count);";
            }
            // array_walk($products_array, function($product, $i) use(&$sql_update, $user_id){
            //     $product_id = $product['product_id'];
            //     $action_code = $product['action_code'];
            //     $action_count = $product['action_count'];
            //     $sql_update .= "replace into T_PRODUCT_LINK_INFO(product_id, action_code, user_id, action_count) values('$product_id', '$action_code', '$user_id', $action_count);";
            // });
        }
        else{
            $product = json_decode($products_json, true);
            $product_id = $product['product_id'];
            $action_code = $product['action_code'];
            $action_count = $product['action_count'];
            $sql_update .= "replace into T_PRODUCT_LINK_INFO(product_id, action_code, user_id, action_count) values('$product_id', '$action_code', '$user_id', $action_count);";
        }   
        return Tools::trans_sql($sql_update);
    }
    function add_product_to_database($products_json)
    {
        $sql_insert = "";
        if(Tools::json_is_array($products_json)){
            $products_array = json_decode($products_json, true);
            foreach ($products_array as $key => $product) {
                $product_id = $product['product_id'];
                $product_name = $product['product_name'];
                $note = $product['note'];               
                $quantity = $product['quantity'];               
                $sql_insert .= "replace into T_PRODUCT(product_id, product_name, note, quantity) 
                    values('$product_id', '$product_name', '$note', $quantity);";
            }
            // array_walk($products_array, function($product, $i) use(&$sql_insert){
            //     $product_id = $product['product_id'];
            //     $product_name = $product['product_name'];
            //     $note = $product['note'];               
            //     $quantity = $product['quantity'];               
            //     $sql_insert .= "replace into T_PRODUCT(product_id, product_name, note, quantity) 
            //         values('$product_id', '$product_name', '$note', $quantity);";
            // });            
        }
        else{
            $product = json_decode($products_json, true);
            $product_id = $product['product_id'];
            $product_name = $product['product_name'];
            $note = $product['note'];               
            $quantity = $product['quantity'];               
            $sql_insert .= "replace into T_PRODUCT(product_id, product_name, note, quantity) 
                values('$product_id', '$product_name', '$note', $quantity);";
        }

        return Tools::trans_sql($sql_insert);
    }
    public function remove_product_from_database($products_json)
    {
        $sql_delete = "";
        if(Tools::json_is_array($products_json)){
            $products_array = json_decode($products_json, true);
            foreach ($products_array as $key => $product) {
                $product_id = $product['product_id'];
                $sql_delete .= "delete from T_PRODUCT where product_id = '$product_id';";
                $sql_delete .= "delete from T_PRODUCT_LINK_INFO 
                    where product_id = '$product_id';";
            }
            // array_walk($products_array, function($product, $i) use(&$sql_delete){
            //     $product_id = $product['product_id'];
            //     $sql_delete .= "delete from T_PRODUCT where product_id = '$product_id';";
            //     $sql_delete .= "delete from T_PRODUCT_LINK_INFO 
            //         where product_id = '$product_id';";
            // });
        }
        else{
            $products = json_decode($products_json, true);
            $product_id = $product['product_id'];
            $sql_delete .= "delete from T_PRODUCT where product_id = '$product_id';";
            $sql_delete .= "delete from T_PRODUCT_LINK_INFO 
                where product_id = '$product_id';";
        }
        return Tools::trans_sql($sql_delete);
    }
    public function get_chart_data_product_cost($product_id, $system_id, $user_id)
    {
        //把各个系统耗费的成本汇总到一起
        // 如果 system 参数为空，则综合所有系统
        if(empty($system_id)){
            $system_list_array = Tools::system_list_array();
        }
        else{
            $system_list_array[] = Tools::get_system_info_by_id(Tools::system_list_array(), $system_id);
        }
        $array_product_cost_list = array();
        foreach ($system_list_array as $key => $system) {
            $array_system_action_cost_list 
                = CostCaculateAction::get_product_final_cost($product_id, $system['id'], $user_id);
            
            $array_new_system_product_cost_list = array();
            foreach ($array_system_action_cost_list as $key => $item) {
                    $item['system_id'] = $system['text'];
                    $array_new_system_product_cost_list[] = $item;
            }
            // $array_new_system_product_cost_list
            //     = array_map(function($item) use($system){
            //         $item['system_id'] = $system['text'];
            //         return $item;
            //     }, $array_system_action_cost_list);
            $array_product_cost_list = array_merge($array_product_cost_list, $array_new_system_product_cost_list);
        }
        foreach ($array_product_cost_list as $key => $item) {
            $pies_data_array[] = "['".$item['action_name']."',".$item['action_cost']."]";
        }
        // $pies_data_array = array_map(function($item){
        //     return "['".$item['action_name']."',".$item['action_cost']."]";
        // }, $array_product_cost_list);

        return '['.implode(',',$pies_data_array).']';

        // $all_action_total_fee = 0;

        // array_walk($array_product_cost_list, function($value, $i) use(&$all_action_total_fee){
        //     $action_cost = $value['action_cost'];
        //     $all_action_total_fee += $action_cost;
        // });

        // $pies = array_map(function($item) use($all_action_total_fee){
        //     $action_cost = $item['action_cost'];
        //     $percent = 0;
        //     if($action_cost > 0)
        //     {
        //         $percent = number_format(100 * $action_cost / $all_action_total_fee, 2);
        //     }
        //     return array('label' => $item['action_name'].'('.$percent.'%)', 
        //                     'value' => (($item['action_cost']) + 0));
        // }, $array_product_cost_list);
        // // var_dump($pies); return;
        // $pies_json = json_encode(array_values($pies));
        // $colors = Tools::randomColors(count($array_product_cost_list));
        
        // return '{
        //   "elements":[
        //     {
        //       "type":      "pie",
        //       "colours":   '
        //       .json_encode($colors).',
        //       "alpha":     0.6,
        //       "border":    2,
        //       "start-angle": 35,
        //       "values" :   '
        //       .$pies_json.'
        //     }
        //   ]
        // }';
    }
    // public function get_product_final_cost($product_id = '', $system_id = '', $user_id = '')
    // {
    //     $array_result = array();
    //     //计算单位作业成本
    //     $array_action_cost = CostCaculateAction::action_final_cost($system_id, $user_id);
    //     // 获取每个产品各个作业的使用量
    //     $a_product_action_count = array();

    //     $sql_select_all_product_action_count
    //          = "select T1.product_id, T1.product_name, T1.note, ifnull(T2.action_count, 0) action_count , T2.action_code 
    //         from T_PRODUCT T1, T_PRODUCT_LINK_SYSTEM T3 
    //          left join T_PRODUCT_LINK_INFO T2  on T1.product_id = T2.product_id 
    //           where T1.product_id = T3.product_id  and T3.system_id = '$system_id' and T1.product_id = '$product_id' and T2.user_id = '$user_id';";
    //     $select_result_all_action_count = Tools::get_query_result($sql_select_all_product_action_count);

    //     $array_action_count = array();
    //     array_walk($select_result_all_action_count, function($row, $key) use(&$array_action_count){
    //         $action_code = $row['action_code'];
    //         $array_action_count[$action_code]['action_code'] = $action_code; 
    //         $array_action_count[$action_code]['action_count'] = $row['action_count'];
    //     });
    //     $array_result = array_map(function($item) use($array_action_count){
    //         $each_work_fee = $item['each_work_fee'];
    //         $action_code =  $item['action_code'];
    //         $unit =  $item['unit'];
    //         $action_cost = 0;
    //         $action_count = 0;
    //         if(array_key_exists($action_code, $array_action_count))
    //         {
    //             $action_count = $array_action_count[$action_code]['action_count'];
    //             $action_cost = $each_work_fee * $action_count;
    //         }
    //         return array('action_code' => $action_code
    //                                 , 'action_name' => $item['action_name']
    //                                 , 'each_work_fee' => ($each_work_fee)
    //                                 , 'action_cost' => ($action_cost)
    //                                 , 'unit' => ($unit)
    //                                 , 'action_count' => $action_count);

    //     }, $array_action_cost);        
    //     return $array_result;
    // }



}
?>