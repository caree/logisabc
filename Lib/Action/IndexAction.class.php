<?php
require_once('CostCaculateAction.class.php');
require_once('tools.php');
class IndexAction extends Action
{
    /**
    +----------------------------------------------------------
    * 输出页面
    +----------------------------------------------------------
    */

    //引导进入子系统
    public function system_nav_index()
    {
        // $this->assign('system_name', $_SESSION['system_name']);
        $this->assign('nick_name', $_SESSION['nick_name']);
        $this->assign('user_account', $_SESSION['acount']);
        $this->assign('company', C('COMPANY_SIGN'));
        $this->assign('version', C('VERSION'));
        $this->display();
    }
    public function index()
    {
        $this->assign('user_account', $_SESSION['acount']);
        $this->assign('nick_name', $_SESSION['nick_name']);
        $this->assign('company', C('COMPANY_SIGN'));
        $this->assign('version', C('VERSION'));
        //都需要区分系统
        $system_id = Tools::request('sys');
        $_SESSION['system_id'] = $system_id;
        $system_name = Tools::get_system_name_by_id_directly($system_id);
        $_SESSION['system_name']= $system_name;
        $this->assign('system_name', $system_name);
        
        switch ($_SESSION['acount']) {
            case 'admin':
                $this->assign('left_menu_admin', $this->tree_admin());
                $this->display('admin_index');
                break;
            case 'teacher':
                if($system_id == 'document'){
                    $this->assign('list', $this->tree_document());
                    $this->display('index_document');
                }
                else{
                    $this->assign('list', $this->tree_teacher());
                    $this->display('index_viewer');
                }
                break;
            
            default:
                if($system_id == 'document'){
                    $this->assign('list', $this->tree_document());
                    $this->display('index_document');
                }
                else{            
                    $this->assign('list', $this->left_menu());
                    $this->display();
                }
                break;
        }
    }
    public function admin_index()
    {
        $this->display();
    }
    public function overview()
    {
        $system_id = Tools::get_system_id();
        $system_info = Tools::get_system_info_by_id_directly($system_id);
        $this->assign('system_name', $system_info['text']);
        $this->assign('system_description', $system_info['description']);
        $this->assign('image_name', $system_info['image']);
        $this->display();
    }
    public function basic_principle()
    {
        $this->display();
    }
    public function ABCcase()
    {
        $this->display();
    }
    public function tree_document()
    {
        $a_tree = array();
        $items_1 = array(
                array('item_name' => '基本原理', turl => '/Index/basic_principle')
                , array('item_name' => '案例分析', turl => '/Index/ABCcase')
            );
        $a_tree[] = array(id => 'subbase_class_manage', name => '文档目录', pId => 0
                        , open => 'true', items => $items_1);

        return $a_tree;
    }
    public function tree_teacher()
    {
        $system_id = Tools::get_system_id();
        $a_tree = array();
        $items_1 = array(
                array('item_name' => '班级管理', turl => '/ClassManage/class_index')
                , array('item_name' => '注册审核', turl => '/UserManagement/UserIndex_not_checked')
                , array('item_name' => '学生管理', turl => '/UserManagement/UserIndex')
            );
        $a_tree[] = array(id => 'subbase_class_manage', name => '用户管理', pId => 0
                        , open => 'true', items => $items_1);

        if($system_id != 'company'){
            $items_2 = array(
                    array('item_name' => '作业设定', turl => '/LogisA/ActionSetting_admin')
                    , array('item_name' => '投入要素类管理', turl => '/InventoryGroup/inventory_group_index')
                    , array('item_name' => '产品管理', turl => '/ProductManagement/ProductIndex_admin')
                );
        }
        else{
            $items_2 = array(
                    array('item_name' => '产品管理', turl => '/ProductManagement/ProductIndex_admin')
                );            
        }

        $a_tree[] = array(id => 'subbase1', name => '系统参数设置'
                        , open => 'true', items => $items_2);  


        if($system_id == 'company'){
            $items_3 = array(
                        array('item_name' => '总系统作业成本分析结果', turl => '/UserManagement/user_list_for_action_cost_company_system_index')
                        , array('item_name' => '总系统产品作业成本分析结果', turl => '/UserManagement/user_list_for_product_cost_company_system_index')
                    );           
        }
        else{
            $items_3 = array(
                        array('item_name' => '作业成本分析结果', turl => '/UserManagement/user_list_for_action_cost_index')
                        , array('item_name' => '产品作业成本分析结果', turl => '/UserManagement/user_list_for_product_cost_index')
                    );               
        }
        $a_tree[] = array(id => 'subbase1', name => '学生作业成本分析结果'
                        , open => 'true', items => $items_3);                                
        // $this->assign('list', $a_tree);
        return $a_tree;

        return json_encode(array_values($a_tree));
        // $a_tree = array();

        // $a_tree[] = array(id => 'subbase_class_manage', name => '用户管理', pId => 0
        //                 , open => 'true');
        // $a_tree[] = array(id => 'subbase_class_manage_sub', name => '班级设置', pId => 'subbase_class_manage'
        //                 , turl => '/index.php/ClassManage/class_index');
        // $a_tree[] = array(id => 'subbase_user_check_sub', name => '注册审核', pId => 'subbase_class_manage'
        //                 , turl => '/index.php/UserManagement/UserIndex_not_checked');
        // $a_tree[] = array(id => 'subbase_user_manage_sub', name => '学生管理', pId => 'subbase_class_manage'
        //                 , turl => '/index.php/UserManagement/UserIndex');

        // $a_tree[] = array(id => 'subbase1', name => '系统参数设置', pId => 0, open => 'true');
        // if($system_id != 'company'){
            
        //     $a_tree[] = array(id => 'subbase1_sub1', name => '作业设定', pId => 'subbase1', turl => '/index.php/LogisA/ActionSetting_admin');
        //     $a_tree[] = array(id => 'node_subbase_resource_group_manage', name => '投入要素类管理', pId => 'subbase1', 
        //                         turl => '/index.php/InventoryGroup/inventory_group_index');
        // }

        // $a_tree[] = array(id => 'node_product_index_sub', name => '产品管理', pId => 'subbase1', 
        //                     turl => '/index.php/ProductManagement/ProductIndex_admin');



        // $a_tree[] = array(id => 'node_stastic_result', name => '学生统计结果', pId => 0, open => 'true');
        
        // if($system_id == 'company'){
        //     $a_tree[] = array(id => 'sub_node_action_result', name => '总系统作业成本统计', pId => 'node_stastic_result', 
        //                         turl => '/index.php/UserManagement/user_list_for_action_cost_company_system_index');
        //     $a_tree[] = array(id => 'sub_node_production_result', name => '总系统产品作业成本统计', pId => 'node_stastic_result', 
        //                         turl => '/index.php/UserManagement/user_list_for_product_cost_company_system_index');

        // }
        // else{
        //     $a_tree[] = array(id => 'sub_node_action_result', name => '作业成本统计', pId => 'node_stastic_result', 
        //                         turl => '/index.php/UserManagement/user_list_for_action_cost_index');
        //     $a_tree[] = array(id => 'sub_node_production_result', name => '产品作业成本统计', pId => 'node_stastic_result', 
        //                         turl => '/index.php/UserManagement/user_list_for_product_cost_index');
        // }

    }  
    public function tree_admin()
    {
        $system_id = Tools::get_system_id();
        $a_tree = array();

        $a_tree[] = array(id => 'node_user_index', name => '用户管理', pId => 0, open => 'true');
        $a_tree[] = array(id => 'node_user_index_sub', name => '用户管理', pId => 'node_user_index', turl => '/index.php/UserManagement/UserIndex');

        return json_encode(array_values($a_tree)); 
        return;
    }
   public function left_menu()
    {
        $system_id = Tools::get_system_id();
        if($system_id == 'company'){
            $items_1 = array(
                    array('item_name' => '当前作业成本分析结果', turl => '/LogisA/action_final_cost_company_system_index_user')
                    , array('item_name' => '历史作业成本分析结果', turl => '/CostDataHistory/company_action_cost_data_history_index')
                );            

            $a_tree[] = array(id => 'subbase4', name => '总系统作业成本分析结果', 
                                items => $items_1);

            $items_2 = array(
                    array('item_name' => '当前产品作业成本分析结果', turl => '/ProductManagement/product_index_for_company_cost_result')
                    , array('item_name' => '历史产品作业成本分析结果', turl => '/CostDataHistory/company_product_cost_data_history_index')
                );            

            $a_tree[] = array(id => 'subbase5', name => '总系统产品作业成本分析结果', 
                                items => $items_2);            
        }
        else{
            $items_3 = array(
                    array('item_name' => '作业处理量赋值', turl => '/LogisA/ActionSetting_user')
                    , array('item_name' => '投入要素费用赋值', turl => '/InventoryGroup/inventory_group_for_user_resource_fee_set_index')
                    , array('item_name' => '投入要素处理比例设定', turl => '/InventoryGroup/user_inventory_group_for_resource_percent_set_index')
                    , array('item_name' => '产品处理量分配', turl => '/ProductManagement/product_action_count_setting_index')
                );            

            $a_tree[] = array(id => 'subbase1', name => '系统参数赋值', 
                                items => $items_3);     
                                
            $items_4 = array(
                    array('item_name' => '当前作业成本分析结果', turl => '/LogisA/list_action_final_cost_index')
                    , array('item_name' => '历史作业成本分析结果', turl => '/CostDataHistory/cost_data_history_index')
                );            

            $a_tree[] = array(id => 'subbase4', name => '作业成本分析结果', 
                                items => $items_4);    
            $items_5 = array(
                    array('item_name' => '当前产品作业成本分析结果', turl => '/ProductManagement/product_index_for_cost_result')
                    , array('item_name' => '历史产品作业成本分析结果', turl => '/CostDataHistory/product_cost_data_history_index')
                );            

            $a_tree[] = array(id => 'subbase5', name => '产品作业成本分析结果', 
                                items => $items_5);    

        }

        // $this->assign('list', $a_tree);
        return $a_tree;
        
        $a_tree = array();
        if($system_id == 'company'){

            $a_tree[] = array(id => 'subbase4', name => '总系统作业成本统计', pId => 0, open => 'true');
            $a_tree[] = array(id => 'node_show_currentfinal_cost', 
                                name => '当前作业成本分析结果', pId => 'subbase4',
                                turl => '/index.php/LogisA/action_final_cost_company_system_index_user');        
                                // turl => '/index.php/Index/list_action_final_cost_index');        
            $a_tree[] = array(id => 'node_final_cost_history', 
                                name => '历史作业成本分析结果', pId => 'subbase4',
                                turl => '/index.php/CostDataHistory/company_action_cost_data_history_index');             

            $a_tree[] = array(id => 'subbase5', name => '总系统产品成本统计', pId => 0, open => 'true');

            $a_tree[] = array(id => 'sub2_subbase5', name => '当前产品成本统计结果', pId => "subbase5", 
                                turl => '/index.php/ProductManagement/product_index_for_company_cost_result');
            $a_tree[] = array(id => 'sub3_subbase5', name => '历史产品成本分析结果', pId => "subbase5", 
                                turl => '/index.php/CostDataHistory/company_product_cost_data_history_index');
        }
        else{
            $a_tree[] = array(id => 'subbase1', name => '系统参数赋值', pId => 0, open => 'true');
            
            // $a_tree[] = array(id => 'subbase1', name => '作业赋值', pId => 0, open => 'true');
            $a_tree[] = array(id => 'subbase1_sub1', name => '作业处理量赋值', pId => 'subbase1', 
                                turl => '/index.php/LogisA/ActionSetting_user');

            $a_tree[] = array(id => 'subbase2', name => '投入要素费用赋值', pId => 'subbase1', 
                                turl => '/index.php/InventoryGroup/inventory_group_for_user_resource_fee_set_index');

            $a_tree[] = array(id => 'subbase3', name => '投入要素处理比例设定', pId => 'subbase1', 
                                turl => '/index.php/InventoryGroup/user_inventory_group_for_resource_percent_set_index');
            $a_tree[] = array(id => 'sub1_subbase5', name => '产品处理量分配', pId => "subbase1", 
                                turl => '/index.php/ProductManagement/product_action_count_setting_index');

            $a_tree[] = array(id => 'subbase4', name => '作业成本分析结果', pId => 0, open => 'true');
            $a_tree[] = array(id => 'node_show_currentfinal_cost', 
                                name => '当前作业成本分析结果', pId => 'subbase4',
                                turl => '/index.php/LogisA/list_action_final_cost_index');        
                                // turl => '/index.php/Index/list_action_final_cost_index');        
            $a_tree[] = array(id => 'node_final_cost_history', 
                                name => '历史作业成本分析结果', pId => 'subbase4',
                                turl => '/index.php/CostDataHistory/cost_data_history_index');             

            $a_tree[] = array(id => 'subbase5', name => '产品成本统计', pId => 0, open => 'true');

            $a_tree[] = array(id => 'sub2_subbase5', name => '当前产品成本统计结果', pId => "subbase5", 
                                turl => '/index.php/ProductManagement/product_index_for_cost_result');
            $a_tree[] = array(id => 'sub3_subbase5', name => '历史产品成本分析结果', pId => "subbase5", 
                                turl => '/index.php/CostDataHistory/product_cost_data_history_index');

        }


        return json_encode(array_values($a_tree)); 
    }
 
    //查看作业成本
    //因为要统计每类投入要素的成本，这里把值传递给页面
    public function list_action_final_cost_index()
    {
        $a_items = array();

        $list_all_inventory_resource_groups = $this->get_all_system_inventory_resource_groups();
        for($i = 0; $i < count($list_all_inventory_resource_groups); $i++)
        {
            $group = $list_all_inventory_resource_groups[$i];

            $a_items[$i]['field'] = $group['resource_group_id'].'_fee';
            $a_items[$i]['value'] = $group['resource_group_name'];
        }

        $this->assign('list', $a_items);
        $this->display();
    }
    public function export_result_excel(){

        // $array_result = $this->caculate_final_cost();
        $list_all_inventory_resource_groups = $this->get_all_system_inventory_resource_groups();
        // var_dump($list_all_inventory_resource_groups); return;
        Vendor("PHPExcel.PHPExcel"); 
        $objPHPExcel = new PHPExcel();
        //设置列标题
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', '作业编码')
                                            ->setCellValue('B1','作业名称')
                                            ->setCellValue('c1', '总作业成本')
                                            ->setCellValue('d1', '月处理量')
                                            ->setCellValue('e1','单位处理成本');
        $a_new_column_num = Tools::number_to_ABC_map('f', count($list_all_inventory_resource_groups));
        $a_column_resource_group_id_map = array();
        for($i = 0; $i < count($a_new_column_num); $i++)
        {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($a_new_column_num[$i].'1', $list_all_inventory_resource_groups[$i]['resource_group_name']);
            $a_column_resource_group_id_map[$a_new_column_num[$i]] = $list_all_inventory_resource_groups[$i]['resource_group_id'];
        }
        $a_all_column_num = Tools::number_to_ABC_map('a', 5 + count($list_all_inventory_resource_groups));

        //输入数据
        $array_result = $this->action_final_cost();
        // var_dump($array_result);return;
        // for($i = 0; $i < count($array_result); $i++)
        $i = -1;
        foreach ($array_result as $key => $value) {
            $i ++;
            for($j = 0; $j < count($a_all_column_num); $j++)
            {
                $column_index = $a_all_column_num[$j];
                $row_index = $i + 2;
                switch ($column_index) {
                    case 'A':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_code']);
                        break;
                    case 'B':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_name']);
                        break;
                    case 'C':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_total_fee']);
                        break;
                    case 'D':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['action_work_capability']);
                        break;
                    case 'E':
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value['each_work_fee']);
                        break;
                    default:
                        $group_id = $a_column_resource_group_id_map[$column_index];
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column_index.$row_index, $value[$group_id.'_fee']);
                        break;
                }
            }
        }

        date_default_timezone_set("Asia/Shanghai");
        $time= date("Y-m-d");
        $xls_name = 'report_'.$time;
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$xls_name.xls");
        //header('Content-Disposition: attachment;filename="ex.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }    
    public function get_chart_data()
    {
        $result = $this->action_final_cost();
        // var_dump($result); return;
        $all_action_total_fee = 0;
        foreach ($result as $key => $item) {
            $all_action_total_fee += $item['action_total_fee'];
        }
        $pies = array();
        foreach ($result as $key => $item) {
            $action_total_fee = $item['action_total_fee'];
            $percent = 0;
            if($action_total_fee > 0)
            {
                $percent = number_format(100 * $action_total_fee / $all_action_total_fee, 2);
            }
            $pies[] = array('label' => $item['action_name'].'('.$percent.'%)', 
                            'value' => ($item['action_total_fee'] + 0));
        }
        // var_dump($pies); return;
        $pies_json = json_encode($pies);

        $colors = Tools::randomColors(count($result));
        echo '{
          "elements":[
            {
              "type":      "pie",
              "colours":   '
              .json_encode($colors).',
              "alpha":     0.6,
              "border":    2,
              "start-angle": 35,
              "values" :   '
              .$pies_json.'
            }
          ]
        }';
    }

    public function save_data()
    {
        // $result['status'] = 'ok';
        $json = Tools::request_json("data");
        // $user_id = Tools::get_user_data_set();
        // $flag = true;
        if(count($json) > 0) {
            $flag = $this->save_history_data($json);
            if ($flag == false) {
                $result = Tools::set_result_json('failed', '插入数据时出现异常');
            }
            else
            {
                $result = Tools::set_result_json('ok', '数据保存为 '.$data_id.', 请在作业成本历史页面查看。');
            }
        }
        else
        {
            $result = Tools::set_result_json('failed', '没有数据被保存！');
        }
        echo $result;  
    }   
    public function list_action_final_cost_list()
    {
        $result = $this->action_final_cost();
        // var_dump($result);
        echo json_encode(array_values($result));
        return;
    } 
    /**
    +----------------------------------------------------------
    * 数据计算输出
    +----------------------------------------------------------
    */
    public function save_history_data($json)
    {
        if(!empty($json)){
            $user_id = Tools::get_user_data_set();
            // $flag = true;
            if(count($json) > 0){
                //以日期作为键值，检查现有保存数据，则替换已有的
                date_default_timezone_set("Asia/Shanghai");
                // $time = date("Y-m-d G:i:s");
                $time = date("Y-m-d H:i:s");
                $data_id= 'report_'.date("YmdHis");
                $sql_replace = "replace into T_ACTION_COST_HISTORY(history_id, user_id, time_stamp) values('$data_id', '$user_id', '$time');";
                foreach ($json as $key => $item) {
                    $action_code = $item['action_code'];
                    $action_total_fee = str_replace(',', '', $item['action_total_fee']);
                    $sql_replace .= "replace into T_ACTION_LINK_HISTORY(history_id, action_code, user_id, action_fee) values('$data_id', '$action_code', '$user_id', $action_total_fee);";
                }
                // array_walk($json, function($item, $i) use(&$sql_replace){
                //     $action_code = $item['action_code'];
                //     $action_total_fee = str_replace(',', '', $item['action_total_fee']);
                //     $sql_replace .= "replace into T_ACTION_LINK_HISTORY(history_id, action_code, user_id, action_fee) values('$data_id', '$action_code', '$user_id', $action_total_fee);";
                // });
                return Tools::trans_sql($sql_replace);            
            }
            else{
                return false;
            }
        }
    }
    public function action_final_cost()
    {
        $system_id = Tools::get_system_id();
        $user_data_set = Tools::get_user_data_set();
        $result = CostCaculateAction::action_final_cost($system_id, $user_data_set);
        return $result;        
    }

    public function get_all_system_inventory_resource_groups()
    {
        $system_id = Tools::get_system_id();
        $sql_select_all_inventory_resource_groups = 
            "select resource_group_id, resource_group_name, system_id from T_INVENTORY_RESOURCE_GROUP where system_id = '$system_id';";
        $list_all_inventory_resource_groups = Tools::get_query_result($sql_select_all_inventory_resource_groups);
        return $list_all_inventory_resource_groups;
    }


}
?>