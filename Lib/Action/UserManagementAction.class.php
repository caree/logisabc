<?php
require_once('tools.php');

class UserManagementAction extends Action
{
    /**
    +----------------------------------------------------------
    * 页面输出
    +----------------------------------------------------------
    */
	public function register_user_index()
	{
        $this->assign('version', C('VERSION'));
        $this->assign('company', C('COMPANY_SIGN'));
		$this->display();
	}
	public function register_user()
	{
        $json = Tools::request_json("data");
        
		$result['status'] = 'ok';
        $user_name = $json['user_name'];
        $user_id = $json['user_id'];
        $pwd = $json['new_psw1'];
        $class_id = $json['class_id'];

        if($this->check_new_user_if_already_exits($user_id)){
            $foo_json = Tools::set_result_json('failed', '该学号已经注册！');
        }
        else{
            $r = $this->add_user_to_database($user_id, $user_name, $pwd, $class_id);
            if($r){
                $foo_json = Tools::set_result_json('ok', '注册成功');
            }
            else{
                $foo_json = Tools::set_result_json('failed', '注册时出现异常！');
            }
        }
        echo $foo_json;        
	}

	public function change_pwd_index()
	{
		$this->display();
	}
	public function updatepwd()
	{
		$crtUser = $_SESSION['acount'];
        $json = Tools::request_json("data");
        $oldpwd = $json['old_psw'];
        // echo $oldpwd; return;
        $new_psw = $json['new_psw1'];

	    if($this->check_crt_pwd_right($crtUser, $oldpwd)){
            $r = $this->update_pwd($crtUser, $new_psw);
            if($r){
                $foo_json = Tools::set_result_json('ok', '更改密码成功！');
            }
            else{
                $foo_json = Tools::set_result_json('failed', '更改密码失败！');
            }
        }	
        else{
            $foo_json = Tools::set_result_json('failed', '输入的当前密码错误！');
        }
        echo $foo_json;
	}

	public function UserIndex()
	{
        $sql = "select class_id, class_name from T_CLASSES order by create_time asc;";
        $result = Tools::get_query_result($sql);
        $this->assign('classes', json_encode($result));
		$this->display();		
	}
    //设置注册是否通过
	public function check_user()
	{
        $json = Tools::request_json("data");
        $flag = $this->set_register_user_checked($json);
        echo $flag ? 'ok':'failed';
	}

	public function update_user_info()
	{
        $json = Tools::request("data");
        $flag = $this->update_user_info_to_database($json);
        echo $flag ? 'ok':'failed';
	}

	public function reset_pwd()
    {
        $json = Tools::request("data");
        $flag = $this->reset_pwd_default_to_database($json);
        echo $flag ? 'ok':'failed';
	}

	public function user_list_unchecked()
	{
		echo $this->user_list_with_para('no');
	}
	public function user_list()
	{
		echo $this->user_list_with_para('yes');
	}
    public function remove_user()
    {
        $json = Tools::request("data");
        $flag = $this->remove_user_from_database($json);
        echo $flag ? 'ok':'failed';
    }
    public function user_list_for_product_cost_company_system_index()
    {
        $sql = "select class_id, class_name from T_CLASSES order by create_time asc;";
        $result = Tools::get_query_result($sql);
        $this->assign('classes', json_encode($result));          
        $this->display();
    }
    public function user_list_for_action_cost_company_system_index()
    {
        $sql = "select class_id, class_name from T_CLASSES order by create_time asc;";
        $result = Tools::get_query_result($sql);
        $this->assign('classes', json_encode($result));          
        $this->display();
    }
    //显示一个用户学生列表，作为选择作业成本的入口
    public function user_list_for_action_cost_index()
    {
        $sql = "select class_id, class_name from T_CLASSES order by create_time asc;";
        $result = Tools::get_query_result($sql);
        $this->assign('classes', json_encode($result));        
        $this->display();
    }
    public function user_list_for_product_cost_index()
    {
        $sql = "select class_id, class_name from T_CLASSES order by create_time asc;";
        $result = Tools::get_query_result($sql);
        $this->assign('classes', json_encode($result));         
        $this->display();
    }
    /**
    +----------------------------------------------------------
    * 数据计算输出
    +----------------------------------------------------------
    */
    function remove_user_from_database($users_json)
    {
        $user_list[] = array();
        if(Tools::json_is_array($users_json)){
            $users_array = json_decode($users_json,true);
            foreach ($users_array as $key => $user) {
                $user_list[] = $user['account'];
            }
            // $user_list = array_map(function($user){
            //     return $user['account'];
            // }, $users_array);
        }
        else{
            $user = json_decode($users_json,true);
            $user_list[] = $user['account'];
        }
        $sql_delete = $this->get_sql_remove_user($user_list);
        return Tools::trans_sql($sql_delete);
    }
    function reset_pwd_default_to_database($users_json)
    {
        $users = "''";
        if(Tools::json_is_array($users_json)){
            $users_array = json_decode($users_json, true);
            foreach ($users_array as $key => $user) {
                $account = $user['account'];
                $users .= ",'".$account."'";
            }
            // array_walk($users_array, function($user, $i) use(&$users){
            //     $account = $user['account'];
            //     $users .= ",'".$account."'";
            // });
        }
        else{
            $user = json_decode($users_json, true);;
            $account = $user['account'];
            $users .= ",'".$account."'";            
        }

        $default_secret = Tools::get_deault_secret();
        $sql_update = "update THINK_USER set PASSWORD = '$default_secret' where ACCOUNT in($users);";
        return Tools::trans_sql($sql_update);
    }
    function update_user_info_to_database($users_json)
    {
        $sql_update = "";
        if(Tools::json_is_array($users_json)){
            $users_array = json_decode($users_json, true);
            foreach ($users_array as $key => $user) {
                $account = $user['account'];
                $nick_name = $user['remark'];
                $class_id = $user['class_id'];
                $sql_update .= "update THINK_USER set REMARK = '$nick_name' where ACCOUNT = '$account';";
                $sql_update .= "update T_CLASS_LINK_USER set class_id = '$class_id' where user_id = '$account';";
            }
            // array_map(function($user) use(&$sql_update){
            //     $account = $user['account'];
            //     $nick_name = $user['remark'];
            //     $class_id = $user['class_id'];
            //     $sql_update .= "update THINK_USER set REMARK = '$nick_name' where ACCOUNT = '$account';";
            //     $sql_update .= "update T_CLASS_LINK_USER set class_id = '$class_id' where user_id = '$account';";
            //     // return $sql_update;
            // }, $users_array);            
        }
        else{
            $user = json_decode($users_json, true);
            $account = $user['account'];
            $nick_name = $user['remark'];
            $class_id = $user['class_id'];
            $sql_update .= "update THINK_USER set REMARK = '$nick_name' where ACCOUNT = '$account';";
            $sql_update .= "update T_CLASS_LINK_USER set class_id = '$class_id' where user_id = '$account';";
        }

        return Tools::trans_sql($sql_update);
    }

    function set_register_user_checked($users_array)
    {
        $users = "''";
        foreach ($users_array as $key => $user) {
            $account = $user['account'];
            $users .= ",'".$account."'";
        }
        // array_walk($users_array, function($user, $i) use(&$users){
        //     $account = $user['account'];
        //     $users .= ",'".$account."'";
        // });
        $sql_update = "update THINK_USER set status = 'yes' where ACCOUNT in($users);";
        return Tools::trans_sql($sql_update);
    }

    function update_pwd($user_name, $new_psw)
    {
        $newpwdMd5 = md5($new_psw);
        $sqlUpdate = "UPDATE THINK_USER SET PASSWORD = '$newpwdMd5' where ACCOUNT = '$user_name';";
        return Tools::trans_sql($sqlUpdate);
    }
    function check_crt_pwd_right($user_name, $oldpwd)
    {
        $oldpwdMd5 = md5($oldpwd);
        $sql = "SELECT USERS.ACCOUNT FROM THINK_USER USERS WHERE  USERS.ACCOUNT = '$user_name' and PASSWORD = '$oldpwdMd5';";
        $list = Tools::get_query_result($sql);
        return (count($list) <= 0) ? false:true;
    }
    function add_user_to_database($user_id, $user_name, $pwd, $class_id)
    {
        $pwdMd5 = md5($pwd);
        $sql_insert = "insert into THINK_USER(ACCOUNT, PASSWORD, REMARK) values('$user_id', '$pwdMd5', '$user_name');";
        $sql_insert .= "insert into T_CLASS_LINK_USER(class_id, user_id) values('$class_id','$user_id');";
        return Tools::trans_sql($sql_insert);
    }
    public function check_new_user_if_already_exits($user_name)
    {
        if (!empty($user_name)) {
            $sql = "SELECT ACCOUNT FROM THINK_USER where ACCOUNT = '$user_name';";
            $list = Tools::get_query_result($sql);
            //该用户名尚未注册
            if(count($list) > 0 ){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    }
	// $check = yes | no
	public function user_list_with_para($check)
	{
		$sql = "SELECT distinct  USERS.ACCOUNT, USERS.REMARK,USERS.PASSWORD,class.class_id, class.class_name   
				 FROM THINK_USER USERS, T_CLASS_LINK_USER link, T_CLASSES class 	
				 WHERE   USERS.ACCOUNT NOT IN ('admin', 'teacher') and USERS.status = '$check'
				 and USERS.ACCOUNT = link.user_id and link.class_id = class.class_id;";
		$list = Tools::get_query_result($sql);
		$default_secret = Tools::get_deault_secret();
        $result = array();
        foreach ($list as $key => $item) {
            $temp = $item['PASSWORD'] == $default_secret ? '系统默认':'已修改';
            $result[] = array('account' => $item['ACCOUNT']
                                , 'secret' => $temp
                                , 'remark' => $item['REMARK']
                                , 'class_id' => $item['class_id']
                                , 'class_name' => $item['class_name']);
        }
        // $result = array_map(function($item) use($default_secret){
        //     $temp = $item['PASSWORD'] == $default_secret ? '系统默认':'已修改';
        //     return array('account' => $item['ACCOUNT']
        //                         , 'secret' => $temp
        //                         , 'remark' => $item['REMARK']
        //                         , 'class_id' => $item['class_id']
        //                         , 'class_name' => $item['class_name']);

        // }, $list);
        $foo_json = json_encode($result);
        return $foo_json;	
	}
	public function get_sql_remove_user($user_list)
	{
        $users = "''";
        foreach ($user_list as $key => $value) {
            $users .= ",'".$value."'";
        }
        // array_walk($user_list, function($value, $key) use(&$users){
        //     $users .= ",'".$value."'";
        // });
        // for($i = 0; $i < count($user_list); $i++)
        // {
        //     $users .= ",'".$user_list[$i]."'";
        // }
        $sql_delete = "delete from THINK_USER
                    where ACCOUNT in($users);";
        //删除与该用户相关的数据
        // 作业处理量
        $sql_delete .= "delete from T_ACTION_LINK_WORK_CAPABILITY where user_id in($users);";
        $sql_delete .= "delete from T_INVENTORY_RESOURCE_LINK_USER_LINK_FEE where user_id in($users);";
        $sql_delete .= "delete from T_INVENTORY_RESOURCE_ACTION_USAGE where user_id in($users);";
        $sql_delete .= "delete from T_ACTION_COST_HISTORY where user_id in($users);";
        $sql_delete .= "delete from T_ACTION_LINK_HISTORY where user_id in($users);";
        $sql_delete .= "delete from T_PRODUCT_LINK_INFO where user_id in($users);";

        return $sql_delete;        
	}


}

?>
