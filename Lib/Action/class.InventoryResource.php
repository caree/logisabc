<?php

class InventoryResource
{
    public $resource_id;//编码
    public $resource_name;//名称
    public $resource_note;//说明描述
    public $resource_selected;
    public $resource_fee = 100;//每月费用
    function __construct($resource_id = '', $resource_name = '', $resource_note = '', $resource_selected = '1')
    {
        $this->resource_id = $resource_id;
        $this->resource_name = $resource_name;
        $this->resource_note = $resource_note;
        $this->resource_selected = $resource_selected;
    }
}
/**
* 
*/
class InventoryGroup 
{
    public $inventory_id;
    public $inventory_name;
    public $inventory_note;
    public $inventory_selected;
    function __construct($inventory_id = '', $inventory_name = '', $inventory_note = '')
    {
        $this->inventory_id = $inventory_id;
        $this->inventory_name = $inventory_name;
        $this->inventory_note = $inventory_note;
    }
}

?>