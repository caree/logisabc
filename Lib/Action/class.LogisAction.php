<?php

class LogisAction
{
    public $action_code;
    public $action_name;
    public $action_note;
    public $system_id;
    public $action_selected;
    public $action_work_capability = 0;
    public $action_used_time_length = 0;//每月工作时间
    function __construct($action_code = '', $action_name = '', $action_note = '', $system_id = '')
    {
        $this->action_name = $action_name;
        $this->action_note = $action_note;
        $this->action_code = $action_code;
        $this->system_id = $system_id;
    }
}

class LogisActionWithStatics extends LogisAction
{
    //总活动成本
    public $action_total_fee = 0;
    //一个单位的处理单价
    public $each_work_fee = 0;
    //人事要素费用
    public $human_resourse_fee = 0;
    //空间要素费用
    public $warehouse_fee = 0;
    //机器设备费用
    public $instrument_fee = 0;
    //原材料消耗品费用
    public $material_fee = 0;
    function __construct($action_code = '', $action_name = '', $action_note = '', $action_selected = '1')
    {
        parent::__construct($action_code, $action_name, $action_note, $action_selected);
    }
}

?>