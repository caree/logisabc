<?php
require_once('tools.php');

class WelcomeAction extends Action {
	
	// 首页
	///*
	public function welcome()
	{
        $this->assign('version', C('VERSION'));
        $this->assign('company', C('COMPANY_SIGN'));
        //清空用户信息
        $_SESSION['system_id'] = '';//每次重新登录，重新设置系统
        $_SESSION['acount']= '';
        $_SESSION['nick_name'] = '';

		$this->display();
	}
	public function test_search()
	{
		$M = new Model();
        $sql_select_general_settings = 
        	"select setting_id, setting_value from T_GENERAL_SETTINGS;";
        $list_general_settings = $M->query($sql_select_general_settings);
        $settings = array();

        foreach ($list_general_settings as $index => $value) {
        	$settings[$value['setting_id']] = $value['setting_value'];
        }

        // php versino >= 5.5.0
        // foreach ($list_general_settings as list($key, $value)) {
        // 	$settings[$key] = $value;
        // }

        // var_dump($list_general_settings);
        var_dump($settings);
        // $founded = in_array("system_set", $list_general_settings);

	}

	// 检查标题是否可用
	public function checkLogin()
	{
		$result['status'] = 'ok';
        $json_str = Tools::request("data");
        $json = json_decode($json_str,true);
        $username = $json['user_name'];
        $password = $json['password'];
        // $system_id = $json['system'];
        //查看是否有通用设置
		$M = new Model();
        $sql_select_general_settings = 
        	"select setting_id, setting_value from T_GENERAL_SETTINGS;";
        $list_general_settings = $M->query($sql_select_general_settings);

        $settings = array();
        foreach ($list_general_settings as $index => $value) {
        	$settings[$value['setting_id']] = $value['setting_value'];
        }

        // $_SESSION['system_id'] = $system_id;
        // $system_name = Tools::get_system_name_by_id(Tools::system_list_array(), $system_id);

        // if(array_key_exists('system_set', $settings) && $settings['system_set'] == '_default')
        // {
        // 	//默认使用用户选择的系统设置
	       //  $_SESSION['system_name']= $system_name;
        // }
        // else//系统指定使用具体的系统设置
        // {
	       //  $_SESSION['system_name']= $settings['system_set'];
        // }
        
        if(array_key_exists('user_data_set', $settings) && $settings['user_data_set'] == '_default')
        {
	        $_SESSION['user_data_set']= $username;//默认使用以用户名为标记的数据
        }
        else//系统设置使用具体用户名为标记的数据
        {
	        $_SESSION['user_data_set']= $settings['user_data_set'];
        }
        $_SESSION['system_id'] = '';//每次重新登录，重新设置系统
		$_SESSION['acount']= $username;

        // || $username == 'admin'
        //教师或者管理员单独对待
        if($username == 'teacher')
        {
            $pwdMd5 = md5($password);
            $sql = "SELECT ACCOUNT, REMARK,status 
                    FROM THINK_USER  where 
                    ACCOUNT = '$username'
                     AND PASSWORD = '$pwdMd5'";
            $list = Tools::get_query_result($sql);
            if (count($list) > 0) {
                $result['status'] = 'ok';
                $result['url'] = '/Index/system_nav_index';
                $foo_json = json_encode($result);                
                $_SESSION['nick_name'] = '教师';
            }
            else{
                $foo_json = Tools::set_result_json('failed', '用户名或者密码错误！');
            }
            echo $foo_json;return;
        }
		if (!empty($username) && !empty($password))
		{
			$pwdMd5 = md5($password);
			$M = new Model();
			$sql = "SELECT ACCOUNT, REMARK,status 
					FROM THINK_USER  where 
					ACCOUNT = '$username'
					 AND PASSWORD= '$pwdMd5'";
			$list = $M->query($sql);
			// var_dump($list); return;
			// $host = $_SERVER['HTTP_HOST'];		
            // if($username != 'admin' || $password != 'admin')
			if (count($list) <= 0){
                $foo_json = Tools::set_result_json('failed', '用户名或者密码错误！');
			}
            else{
                $check_status = $list[0]['status'];
                if($check_status == 'no'){
                    $foo_json = Tools::set_result_json('failed', '您尚未审核通过！');
                }
                else{
                    $result['status'] = 'ok';
                    if($username == 'admin' || $username == 'teacher')
                    {
                        $result['url'] = '/Index/index';
                    } 
                    else
                    {
                        $result['url'] = '/Index/system_nav_index';
                    }
                    $foo_json = json_encode($result);
                    $_SESSION['nick_name'] = $list[0]['REMARK'];
                }
            }
        }
        else
        {
            $foo_json = Tools::set_result_json('failed', '用户名或者密码错误！');
        }
        echo $foo_json; 
		return;	
	}
	
}
?>
